package com.janusgraph;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.GraphProperty;
import com.janusgraph.annotation.IndexType;

/**
 * Created by Diablo on 2017/10/26.
 * pm2.5 数据
 */
@GraphLabel
public final class PM25 {

    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"cityIndex", "weatherPM25"})
    @GraphProperty
    public final String city;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Long date;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer pm25;
    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"qualityGradeIndex", "weatherPM25"})
    @GraphProperty
    public final String qualityGrade;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer AQI;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer AQIRank;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer pm10;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer So2;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer No2;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Double Co;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "weatherPM25")
    @GraphProperty
    public final Integer O3;

    public PM25(String city, Long date, Integer pm25, String qualityGrade, Integer AQI, Integer AQIRank, Integer pm10, Integer so2, Integer no2, Double co, Integer o3) {
        this.city = city;
        this.date = date;
        this.pm25 = pm25;
        this.qualityGrade = qualityGrade;
        this.AQI = AQI;
        this.AQIRank = AQIRank;
        this.pm10 = pm10;
        So2 = so2;
        No2 = no2;
        Co = co;
        O3 = o3;
    }

    @Override
    public String toString() {
        return "PM25{" +
                "City='" + city + '\'' +
                ", date=" + date +
                ", pm25=" + pm25 +
                ", qualityGrade='" + qualityGrade + '\'' +
                ", AQI=" + AQI +
                ", AQIRank=" + AQIRank +
                ", pm10=" + pm10 +
                ", So2=" + So2 +
                ", No2=" + No2 +
                ", Co=" + Co +
                ", O3=" + O3 +
                '}';
    }
}
