package com.janusgraph;

import com.janusgraph.util.CreateEdgeImpl;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by shaozhuquan on 2017/11/2.
 */
public class TestCreateEdge {
    static File directory = new File("");
    static String projectPath;

    static {
        try {
            projectPath = directory.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final String CONFIG = projectPath + "\\src\\main\\resources\\hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(TestCreateEdge.class);
    private GraphConnection graphLocalConnection;

    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
    }

    @Test
    public void TestPerson2Admission() {
        String label1 = "person";
        String label2 = "AdmissionBean";
        String edgeLabel = "admissionE";
        CreateEdgeImpl edge = new CreateEdgeImpl(graphLocalConnection.g);
        String[] compareColumns1 = {"idNum"};
        String[] compareColumns2 = {"admissionIdNum"};
        edge.addE(label1, label2, compareColumns1, compareColumns2, edgeLabel);
        List<Edge> edgeList = readElements();
        Assert.assertTrue(edgeList.size()!=0);
    }

    public List<Edge> readElements() {
        List<Edge> results = graphLocalConnection.g.E().hasLabel("admissionE").toList();
        LOGGER.info(results);
        return results;
    }

    @After
    public void over() throws Exception {
        graphLocalConnection.closeGraph();
    }
}