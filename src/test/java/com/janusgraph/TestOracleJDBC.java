package com.janusgraph;

import com.janusgraph.dao.AdmissionMapper;
import com.janusgraph.data.AdmissionBean;
import com.janusgraph.util.OracleUtil;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public class TestOracleJDBC {
    @Test
    public void testConnection() throws UnsupportedEncodingException {
        OracleUtil.getSqlSessionFactory();
    }

    @Test
    public void getAllAdmissionData() {
        AdmissionMapper a = new AdmissionMapper();
        List<AdmissionBean> results = a.getAdmissionBean();
        System.out.println(results.toString());
    }
}
