package com.janusgraph;

import com.janusgraph.file.Csv;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * Created by Diablo on 2017/10/23.
 * 对与Csv的测试类
 */
public class CsvTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(Csv.class);
    private Csv csv1;
    private Csv csv2;
    private Csv csv3;

    @Before
    public void init() throws IOException {
        InputStream in0 = Thread.currentThread().getContextClassLoader().getResourceAsStream("居民个人基本信息抽样2.csv");
        InputStream in1 = Thread.currentThread().getContextClassLoader().getResourceAsStream("居民个人基本信息抽样2.csv");
        InputStream in2 = Thread.currentThread().getContextClassLoader().getResourceAsStream("EMR_ADMISSION_RECORD.csv");

        Reader reader0 = new InputStreamReader(in0, "utf-8");
        Reader reader1 = new InputStreamReader(in1, "utf-8");
        Reader reader2 = new InputStreamReader(in2, "GBK");

        csv1 = new Csv(reader0, "ID_NUM", "ID_NUM");
        csv2 = new Csv(reader1, "NAME", "ID_NUM").handleCsv("NAME", "SEX_CODE", "ID_NUM", "BORN_DATE", "FATHER_NAME");
        csv3 = new Csv(reader2, "ID_NUM", "ID");
    }

    @Test
    public void testGetRelation() throws IOException {
        List<Triple<String, String, String>> result = csv1.getRelation(csv2, "father");
        Assert.assertNotNull(result);
        LOGGER.info("The relation of CSV1 and CSV2 {}", result.toString());
    }

    @Test
    public void testGetRow() {
        Assert.assertNotNull(csv2.getRow(3));
        LOGGER.info("The data of CSV2 row 3 {}", csv2.getRow(3));
    }

    @Test
    public void testGetByPK() {
        Assert.assertNotNull(csv1.findRowByPK("370102200804122237"));
        LOGGER.info("370102200804122237 row data {}", csv1.findRowByPK("370102200804122237").toString());
    }

    @Test
    public void testMutilRelation() {
        List<Triple<String, String, String>> result = csv1.getRelation(csv3, "电子病例");
        LOGGER.info("Relation data of csv1 and csv3 {}", result.toString());
    }
}
