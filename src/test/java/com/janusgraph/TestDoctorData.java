package com.janusgraph;

import com.janusgraph.dao.DoctorMapper;
import com.janusgraph.data.Disease;
import com.janusgraph.data.Doctor;
import com.janusgraph.util.GraphHandler;
import com.janusgraph.util.IndexSchemaException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestDoctorData {
    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(TestDiseaseData.class);
    private GraphConnection graphLocalConnection;

    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
    }

    @Test
    public void loadDoctorData() throws IndexSchemaException, IllegalAccessException {
        DoctorMapper doctorMapper = new DoctorMapper();
        List<Doctor> doctors = doctorMapper.getAllDoctors();
        GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(Doctor.class, "search");
        for (Doctor d : doctors) {
            graphHandler.addV(d, false);
        }
    }

    @Test
    public void readElements() {
        LOGGER.info(graphLocalConnection.g.V().hasLabel("Doctor").valueMap(true).toList());
        List<Vertex> results = graphLocalConnection.g.V().hasLabel("Doctor").toList();
        for (Vertex v : results) {
            v.remove();
//            graphLocalConnection.g.V(v).drop();
        }
        graphLocalConnection.g.tx().commit();
    }
}
