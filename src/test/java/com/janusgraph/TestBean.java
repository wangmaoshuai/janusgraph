package com.janusgraph;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.GraphProperty;
import com.janusgraph.annotation.IndexType;
import org.janusgraph.core.schema.Mapping;

/**
 * Created by Diablo on 2017/10/25.
 */
@GraphLabel
public class TestBean {

    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"searchByA", "searchAB"}, Mapping = Mapping.TEXT)
    private String a;
    @GraphIndex(IndexType = {IndexType.MIXED}, IndexName = {"searchAB"})
    private Integer b;

    private String c;

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}
