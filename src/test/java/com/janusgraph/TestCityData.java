package com.janusgraph;

import com.janusgraph.data.City;
import com.janusgraph.data.County;
import com.janusgraph.data.Province;
import com.janusgraph.util.GraphHandler;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class TestCityData {
    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(TestCityData.class);
    private GraphConnection graphLocalConnection;
    private Province shandong = new Province("山东省", "0530—0539、0543、0546、0631—0635", "250000—277000", "370000");
    private City jinan = new City("济南市", "0531", "250000", "370100");
    private County lixia = new County("历下区", "0531", "250014", "370102");
    private County licheng = new County("历城区", "0531", "250100", "370112");
    private County tianqiao = new County("天桥区", "0531", "250000", "370105");
    private County huaiyin = new County("槐荫区", "0531", "250000", "370104");
    private County shizhong = new County("市中区", "0531", "250002", "370103");
    private County changqing = new County("长清区", "0531", "250300", "370113");
    private County pingyin = new County("平阴县", "0531", "250400", "370124");
    private County jiyang = new County("济阳县", "0531", "251400", "370125");
    private County shanghe = new County("商河县", "0531", "251600", "370126");
    private County zhangqiu = new County("章丘市", "0531", "250200", "370181");

    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
    }

    @Test
    public void loadCity() throws Exception {
        GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(Province.class, "search");
        graphHandler.addV(jinan, false);
        graphHandler.addV(lixia, false);
        graphHandler.addV(licheng, false);
        graphHandler.addV(tianqiao, false);
        graphHandler.addV(huaiyin, false);
        graphHandler.addV(shizhong, false);
        graphHandler.addV(pingyin, false);
        graphHandler.addV(jiyang, false);
        graphHandler.addV(shanghe, false);
        graphHandler.addV(zhangqiu, false);
        graphHandler.addV(shandong, false);
        graphHandler.addV(changqing, false);

    }

    @Test
    public void createRelation() {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("Province");
        management.makeEdgeLabel("City");
        management.makeEdgeLabel("County");
        management.commit();
        GraphTraversalSource g = graphLocalConnection.g;
        Vertex shandong1 = g.V().has("city", shandong.city).next();
        Vertex jinan1 = g.V().hasLabel("City").has("city", jinan.city).next();
        Vertex lixia1 = g.V().has("city", lixia.city).next();
        Vertex licheng1 = g.V().has("city", licheng.city).next();
        Vertex tianqiao1 = g.V().has("city", tianqiao.city).next();
        Vertex huaiyin1 = g.V().has("city", huaiyin.city).next();
        Vertex pingyin1 = g.V().has("city", pingyin.city).next();
        Vertex jiyang1 = g.V().has("city", jiyang.city).next();
        Vertex shanghe1 = g.V().has("city", shanghe.city).next();
        Vertex zhangqiu1 = g.V().has("city", zhangqiu.city).next();
        Vertex changqing1 = g.V().has("city", changqing.city).next();
        g.V(shandong1).as("a").V(jinan1).addE("City").from("a").next();
        g.V(jinan1).as("a").V(lixia1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(licheng1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(tianqiao1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(huaiyin1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(pingyin1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(jiyang1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(shanghe1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(zhangqiu1).addE("County").from("a").next();
        g.V(jinan1).as("a").V(changqing1).addE("County").from("a").next();
        g.tx().commit();
    }

    @Test
    public void createPM25Data() throws Exception {
        GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(PM25.class, "search");
        List<String> datagram = FileUtils.readLines(new File("/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/pm25新.csv"), "gbk");
        datagram.remove(0);
        datagram = datagram.stream().filter(x -> x.contains("章丘") || x.contains("济南")).collect(Collectors.toList());
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (String s : datagram) {
            String[] tmp = s.split(",");
            graphHandler.addV(new PM25(tmp[0],
                    sdf.parse(tmp[1]).getTime(),
                    Integer.valueOf(tmp[2]),
                    tmp[3],
                    Integer.valueOf(tmp[4]),
                    Integer.valueOf(tmp[5]),
                    Integer.valueOf(tmp[6]),
                    Integer.valueOf(tmp[7]),
                    Integer.valueOf(tmp[8]),
                    Double.valueOf(tmp[9]),
                    Integer.valueOf(tmp[10])), false);
        }
    }

    @Test
    public void readElements() {
        GraphTraversalSource g = graphLocalConnection.g;
        LOGGER.info(g.V().has("city", shandong.city).outE().toList().toString());
        LOGGER.info(g.V().hasLabel("City").has("city", "济南市").outE("WeatherConditions").count().next());
        LOGGER.info(g.V().hasLabel("Province").has("city", P.eq("山东省")).out("City").next().value("city"));
    }

    @Test
    public void addWetherRelation() {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("WeatherConditions");
        management.commit();
        GraphTraversalSource g = graphLocalConnection.g;
        Vertex jinan1 = g.V().hasLabel("City").has("city", jinan.city).next();
        Vertex zhangqiu1 = g.V().hasLabel("County").has("city", zhangqiu.city).next();
        List<Vertex> wetherJinan = g.V().hasLabel("PM25").has("city", "济南").toList();
        List<Vertex> zhangqiu = g.V().hasLabel("PM25").has("city", "章丘").next(Integer.MAX_VALUE);
        for (Vertex v : wetherJinan) {
            g.V(jinan1).as("a").V(v).addE("WeatherConditions").from("a").next();
        }
        for (Vertex v : zhangqiu) {
            g.V(zhangqiu1).as("a").V(v).addE("WeatherConditions").from("a").next();
        }
        g.tx().commit();
    }
}
