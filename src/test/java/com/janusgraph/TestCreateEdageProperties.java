package com.janusgraph;

import com.janusgraph.data.Company;
import com.janusgraph.data.InvestmentEvent;

import com.janusgraph.util.GraphHandler;
import com.janusgraph.util.IndexSchemaException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.Cardinality;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestCreateEdageProperties {

    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(TestCreateEdageProperties.class);
    private GraphHandler graphHandler;
    private GraphTraversalSource g;
    private GraphConnection graphLocalConnection;

    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
        graphHandler = new GraphHandler(graphLocalConnection.g);
        g = graphLocalConnection.g;
    }

    @Test
    public void createSchema() throws IndexSchemaException {
        graphHandler.createVSchema(Company.class, "search");
        JanusGraphManagement management = graphLocalConnection.getGraph().get().openManagement();
        management.makeEdgeLabel("investment");
        management.makePropertyKey("investmentEvent").dataType(Object.class).cardinality(Cardinality.LIST).make();
        management.commit();
    }

    @Test
    public void createCompany() throws IllegalAccessException {
        Company baidu = new Company("百度", "北京西二旗", "李彦宏");
        Company ali = new Company("阿里", "杭州", "马云");
        Company mobai = new Company("摩拜", "中国北京", "胡玮炜");
        graphHandler.addV(baidu, false);
        graphHandler.addV(ali, false);
        graphHandler.addV(mobai, false);
        g.V().addV("test").property("investmentEvent", "1").property("investmentEvent", "2").next();
        g.tx().commit();
    }

    @Test
    public void createRelation() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        Vertex baidu = g.V().hasLabel("Company").has("companyName", "百度").next();
        Vertex ali = g.V().hasLabel("Company").has("companyName", "阿里").next();
        Vertex mobai = g.V().hasLabel("Company").has("companyName", "摩拜").next();
        InvestmentEvent a = new InvestmentEvent("天使轮投资", format.parse("2016-01-01").getTime(), 1000000.00, "天使轮");
        InvestmentEvent b = new InvestmentEvent("C轮投资", format.parse("2017-01-01").getTime(), 2000000.00, "C轮");
        InvestmentEvent c = new InvestmentEvent("A轮投资", format.parse("2016-06-01").getTime(), 3000000.00, "A轮");
        g.V(baidu).as("a").V(mobai).addE("investment")
                .property("investmentEvent", a.toString())
                .property("investmentEvent", b.toString()).from("a").next();
        g.V(ali).as("a").V(mobai).addE("investment").property("investmentEvent", c.toString()).from("a").next();
        g.tx().commit();
    }

    @Test
    public void readElements() {
        Vertex baidu = g.V().hasLabel("Company").has("companyName", "百度").next();
        LOGGER.info(g.V(baidu).outE("investment").valueMap(true).toList());
        LOGGER.info(g.V().hasLabel("test").valueMap(true).toList());
    }
}
