package com.janusgraph;

import com.janusgraph.local.LocalApp;
import com.janusgraph.util.GraphHandler;
import com.janusgraph.util.IndexSchemaException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Diablo on 2017/9/28.
 */
public class testLocalGraph {
    private static final Logger LOGGER = Logger.getLogger(testLocalGraph.class);
    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";

    /**
     * NOTE:这个测试会把整个库删除
     */
    @Test
    public void createSchema() {
        LocalApp localApp = new LocalApp(CONFIG);
        try {
            localApp.openGraph();
//            localApp.createSchema();
//            localApp.createElements();
//            localApp.readElements();
            localApp.dropGraph();
            localApp.closeGraph();
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("The graph open fail");
        } finally {
            try {
                localApp.closeGraph();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void createSchemaByAnnotation() throws IndexSchemaException, ConfigurationException {
        GraphConnection graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
        TestBean testBean0 = new TestBean();
        GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(TestBean.class, "search");
        testBean0.setB(1);
        testBean0.setA("hello world");
        testBean0.setC("wangmsh");

        TestBean testBean1 = new TestBean();
        testBean1.setA("testBean1");
        testBean1.setB(2);
        testBean1.setC("hello world1");
        try {
            graphHandler.addV(testBean0, false);
            graphHandler.addV(testBean1, false);
            Object v = graphLocalConnection.g.V().has("b", 1).values("a").next();
            Object v1 = graphLocalConnection.g.V().has("b", 2).values("a").next();
            LOGGER.info(v.toString());
            LOGGER.info(v1.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createPM25Data() throws Exception {
        GraphConnection graphLocalConnection = new GraphLocalConnection(CONFIG);
        GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(PM25.class, "search");
        List<String> datagram = FileUtils.readLines(new File("/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/pm25新.csv"), "gbk");
        datagram.remove(0);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (String s : datagram) {
            String[] tmp = s.split(",");
            graphHandler.addV(new PM25(tmp[0],
                    sdf.parse(tmp[1]).getTime(),
                    Integer.valueOf(tmp[2]),
                    tmp[3],
                    Integer.valueOf(tmp[4]),
                    Integer.valueOf(tmp[5]),
                    Integer.valueOf(tmp[6]),
                    Integer.valueOf(tmp[7]),
                    Integer.valueOf(tmp[8]),
                    Double.valueOf(tmp[9]),
                    Integer.valueOf(tmp[10])), false);

        }
    }
}
