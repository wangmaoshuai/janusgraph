package com.janusgraph;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestCreateRelation {
    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(TestCreateRelation.class);
    private GraphConnection graphLocalConnection;

    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
    }

    @Test
    public void createAdmission2Disease() {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("DiagnoseDisease");
        management.commit();
        List<Vertex> admissions = graphLocalConnection.g.V().hasLabel("admission").toList();
        for (Vertex v : admissions) {
            LOGGER.info(v.value("admissionDiseaseCode").toString());
            List<Vertex> diseases = graphLocalConnection.g.V().hasLabel("Disease").has("ICD10Code", v.value("admissionDiseaseCode").toString()).toList();
            for (Vertex d : diseases) {
                graphLocalConnection.g.V(v).as("a").V(d).addE("DiagnoseDisease").from("a").next();
            }
        }
        graphLocalConnection.g.tx().commit();
    }

    @Test
    public void createDischarge2Disease() {
        List<Vertex> discharges = graphLocalConnection.g.V().hasLabel("discharge").toList();
        for (Vertex v : discharges) {
            LOGGER.info(v.value("dischargeDiseaseCode").toString());
            List<Vertex> diseases = graphLocalConnection.g.V().hasLabel("Disease").has("ICD10Code", v.value("dischargeDiseaseCode").toString()).toList();
            for (Vertex d : diseases) {
                graphLocalConnection.g.V(v).as("a").V(d).addE("DiagnoseDisease").from("a").next();
            }
        }
        graphLocalConnection.g.tx().commit();
    }

    @Test
    public void createPerson2Admission() {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("AdmissionE");
        management.commit();

        List<Vertex> persons = graphLocalConnection.g.V().hasLabel("person").toList();
        for (Vertex v : persons) {
            LOGGER.info(v.value("idNum").toString());
            List<Vertex> diseases = graphLocalConnection.g.V().hasLabel("admission").has("admissionIdNum", v.value("idNum").toString()).toList();
            for (Vertex d : diseases) {
                graphLocalConnection.g.V(v).as("a").V(d).addE("AdmissionE").from("a").next();
            }
        }
        graphLocalConnection.g.tx().commit();
    }

    @Test
    public void createPerson2Discharged() {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("DischargedE");
        management.commit();

        List<Vertex> persons = graphLocalConnection.g.V().hasLabel("person").toList();
        for (Vertex v : persons) {
            LOGGER.info(v.value("idNum").toString());
            List<Vertex> diseases = graphLocalConnection.g.V().hasLabel("discharge").has("dischargeIdNum", v.value("idNum").toString()).toList();
            for (Vertex d : diseases) {
                graphLocalConnection.g.V(v).as("a").V(d).addE("DischargedE").from("a").next();
            }
        }
        graphLocalConnection.g.tx().commit();
    }

    @Test
    public void createPerson2Outpatient() {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("OutpatientE");
        management.commit();

        List<Vertex> persons = graphLocalConnection.g.V().hasLabel("person").toList();
        for (Vertex v : persons) {
            LOGGER.info(v.value("idNum").toString());
            List<Vertex> diseases = graphLocalConnection.g.V().hasLabel("outpatient").has("outpatientIdNum", v.value("idNum").toString()).toList();
            for (Vertex d : diseases) {
                graphLocalConnection.g.V(v).as("a").V(d).addE("OutpatientE").from("a").next();
            }
        }
        graphLocalConnection.g.tx().commit();
    }

    @Test
    public void createOutpatient2Disease() {
        createRelation("outpatient", "Disease", "outpatientIllnessCode", "ICD10Code", "DiagnoseDisease");
    }

    @Test
    public void createOutpatient2Doctor() {
        createEdgeLabel("AttendingPhysician");
        createRelation("outpatient", "Doctor", "outpatientDoctorIdNum", "doctorCode", "AttendingPhysician");
    }
    @Test
    public void createDischarged2Doctor() {
        createRelation("discharge", "Doctor", "dischargeDoctorCode", "doctorCode", "AttendingPhysician");
    }
    @Test
    public void createAdmission2Doctor() {
        createRelation("admission", "Doctor", "admissionDoctorCode", "doctorCode", "AttendingPhysician");
    }
    @Test
    public void createAdmission2Hospital() {
        createEdgeLabel("TreatmentHospital");
        createRelation("admission", "organization", "admissionOrgCode", "orgCode", "TreatmentHospital");
    }
    @Test
    public void createOutpatient2Hospital() {
        createRelation("outpatient", "organization", "outpatientOrgCode", "orgCode", "TreatmentHospital");
    }
    @Test
    public void createDischarged2Hospital() {
        createEdgeLabel("TreatmentHospital");
        createRelation("discharge", "organization", "dischargeDistrictCode", "distinctCode", "TreatmentHospital");
    }
    @Test
    public void createDoctor2Hospital() {
        createEdgeLabel("WorkOrg");
        createRelation("Doctor", "organization", "doctorOrgCode", "orgCode", "WorkOrg");
    }
    @Test
    public void createOutpatient2OutpatientRecipe() {
        createEdgeLabel("WorkOrg");
        createRelation("outpatient", "outpatientRecipe", "doctorOrgCode", "orgCode", "WorkOrg");
    }
    @Test
    public void readElements() {
//        LOGGER.info(graphLocalConnection.g.V().hasLabel("Disease").has("ICD10Code", "A16.200").toList());
//        LOGGER.info(graphLocalConnection.g.V().hasLabel("admission").valueMap(true).toList());
//        LOGGER.info(graphLocalConnection.g.V().hasLabel("person").valueMap(true).toList());
//        LOGGER.info(graphLocalConnection.g.V().hasLabel("outpatient").valueMap(true).toList());
//        LOGGER.info(graphLocalConnection.g.V().hasLabel("Doctor").valueMap(true).toList());
        LOGGER.info(graphLocalConnection.g.V().hasLabel("organization").valueMap(true).toList());

//        LOGGER.info(graphLocalConnection.g.V().hasLabel("Disease").valueMap(true).toList());
//        LOGGER.info(graphLocalConnection.g.V().hasLabel("admission").has("admissionId", "0000659668").toList());
//          LOGGER.info(graphLocalConnection.g.V().hasLabel("admission").has("admissionId", "0000659668").both("DiagnoseDisease").valueMap(true).toList());
    }

    public void createEdgeLabel(String edgeLabel) {
        JanusGraph graph = graphLocalConnection.getGraph().get();
        JanusGraphManagement management = graph.openManagement();
        management.makeEdgeLabel("edgeLabel");
        management.commit();
    }

    public void createRelation(String Vertex1, String Vertex2, String property1, String property2, String edgeLabel) {
        List<Vertex> persons = graphLocalConnection.g.V().hasLabel(Vertex1).toList();
        for (Vertex v : persons) {
            try {
                LOGGER.info(v.value(property1).toString());
                List<Vertex> diseases = graphLocalConnection.g.V().hasLabel(Vertex2).has(property2, v.value(property1).toString()).toList();
                for (Vertex d : diseases) {
                    graphLocalConnection.g.V(v).as("a").V(d).addE(edgeLabel).from("a").next();
                }
                graphLocalConnection.g.tx().commit();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }
}
