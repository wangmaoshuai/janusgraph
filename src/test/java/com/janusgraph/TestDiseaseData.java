package com.janusgraph;

import com.janusgraph.data.Disease;
import com.janusgraph.data.Province;
import com.janusgraph.util.GraphHandler;
import com.janusgraph.util.IndexSchemaException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

public class TestDiseaseData {
    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(TestDiseaseData.class);
    private GraphConnection graphLocalConnection;

    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
    }

    @Test
    public void load() throws IndexSchemaException, IllegalAccessException {
        Disease zhichang = new Disease("直肠恶性肿瘤", "C20  01");
        Disease dannang = new Disease("胆囊炎", "K81.900");
        Disease jingzhui = new Disease("颈椎病", "M47.802");
        Disease tongjing = new Disease("痛经", "N94.600");
        Disease gaoxueya = new Disease("高血压", "I10.x00");
        Disease qiguanyan = new Disease("慢性支气管炎", "J42.x00");
        Disease ganyinghua = new Disease("肝硬化", "K74.600");
        Disease tangniaobing = new Disease("糖尿病", "E14.900");
        Disease zigongjiliu = new Disease("子宫肌瘤", "D26.900");
        Disease feijiehe = new Disease("肺结核", "A16.200");
        Disease luxianexingzhongliu = new Disease("乳腺恶性肿瘤", "C50.900");
        GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(Disease.class, "search");
        graphHandler.addV(dannang, false);
        graphHandler.addV(zhichang, false);
        graphHandler.addV(jingzhui, false);
        graphHandler.addV(tongjing, false);
        graphHandler.addV(gaoxueya, false);
        graphHandler.addV(qiguanyan, false);
        graphHandler.addV(ganyinghua, false);
        graphHandler.addV(tangniaobing, false);
        graphHandler.addV(zigongjiliu, false);
        graphHandler.addV(feijiehe, false);
        graphHandler.addV(luxianexingzhongliu, false);


    }

    @Test
    public void readElements() {
        LOGGER.info(graphLocalConnection.g.V().hasLabel("Disease").valueMap().next(Integer.MAX_VALUE));
    }
}
