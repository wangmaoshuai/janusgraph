package com.janusgraph;

import com.janusgraph.dao.imethod.IAdmissionMapper;
import com.janusgraph.data.AdmissionBean;
import com.janusgraph.util.GraphHandler;



import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

public class AdmissionBeanTest {
    private static final String CONFIG = "/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/hbase-es.properties";
    private static final Logger LOGGER = Logger.getLogger(AdmissionBeanTest.class);
    private GraphConnection graphLocalConnection;

    @Resource
	private IAdmissionMapper iAdmissionMapper;
    
    @Before
    public void init() throws ConfigurationException {
        graphLocalConnection = new GraphLocalConnection(CONFIG);
        graphLocalConnection.openGraph();
    }

    @Test
    public void admissionBeanTest() throws Exception {
    	
    	GraphHandler graphHandler = new GraphHandler(graphLocalConnection.g);
        graphHandler.createVSchema(AdmissionBean.class, "search");
    	List<AdmissionBean> list = iAdmissionMapper.getAllAdmmissions();
    	if(list != null){
    		for(AdmissionBean admissionBean :list){
    			try {
    	            graphHandler.addV(admissionBean, false);
    	        } catch (Exception e) {
    	            e.printStackTrace();
    	        }
    		}
    	}   
    }

    @After
    public void readElement() throws Exception {
        Object v1 = graphLocalConnection.g.V().has("admissionIdNum", 123456789).values("admissionPresentIllness").next();
        LOGGER.info(v1.toString());
        graphLocalConnection.closeGraph();
    }
}
