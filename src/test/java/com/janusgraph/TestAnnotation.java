package com.janusgraph;

import com.janusgraph.util.GraphHandler;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Diablo on 2017/10/25.
 */
public class TestAnnotation {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestAnnotation.class);
    private GraphTraversalSource g;
    private Client client;

    @Before
    public void init() throws ConfigurationException {
        GraphRemoteConnection graphConnection = new GraphRemoteConnection("D:\\workspace\\janusgraph\\src\\main\\resources\\jgex-remote.properties");
        LOGGER.info("opening graph");
        g = graphConnection.openGraph();
        client = graphConnection.getClient();
    }

    @Test
    public void testAnnotation() throws IllegalAccessException {
        TestBean testBean = new TestBean();
        testBean.setA("hello world");
        testBean.setB(1);
        GraphHandler graphHandler = new GraphHandler(g);
        graphHandler.addV(testBean);
        Object v=g.V().has("b",1).values("a").next();
        LOGGER.info(v.toString());
    }

    @After
    public void destory() {
        try {
            g.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
