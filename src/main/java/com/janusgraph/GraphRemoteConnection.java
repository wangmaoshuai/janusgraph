package com.janusgraph;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.util.empty.EmptyGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Diablo on 2017/10/24.
 * 图数据库远程连接
 */
public class GraphRemoteConnection extends GraphConnection {
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphRemoteConnection.class);
    private Cluster cluster;
    private Client client;

    public GraphRemoteConnection(final String propFileName) {
        super(propFileName);
    }

    @Override
    public GraphTraversalSource openGraph() throws ConfigurationException {
        LOGGER.info("opening graph");
        Configuration conf = new PropertiesConfiguration(this.propFileName);

        try {
            cluster = Cluster.open(conf.getString("gremlin.remote.driver.clusterFile"));
            client = cluster.connect();
        } catch (Exception e) {
            throw new ConfigurationException(e);
        }

        graph = EmptyGraph.instance();
        g = graph.traversal().withRemote(conf);
        return g;
    }

    @Override
    public void closeGraph() throws Exception {
        LOGGER.info("closing graph");
        try {
            if (g != null) {
                g.close();
            }
            if (cluster != null) {
                cluster.close();
            }
        } finally {
            g = null;
            graph = null;
            client = null;
            cluster = null;
        }
    }

    public Client getClient() {
        return this.client;
    }
}
