package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel
public class IchBean {
	
	private String ichId;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "ichOrgCodeAndTolalAmountAndDiseaseCodeIndex")
	private String ichOrgCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "ichAndPerson")
	private String ichOrgName;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "ichOrgCodeAndTolalAmountAndDiseaseCodeIndex")
	private Double ichTotalAmount;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "ichIdNumIndex")
	private String ichIdNum;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "ichAndPerson")
	private String ichDistrictCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "ichAndPerson")
	private String ichNodeCode;
	private String ichDcsId;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "ichOrgCodeAndTolalAmountAndDiseaseCodeIndex")
	private String ichDiseaseCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "ichAndPerson")
	private String ichDisease;
	private Long ichDiagnosisDate;
	
	public String getIchId() {
		return ichId;
	}
	public void setIchId(String ichId) {
		this.ichId = ichId;
	}
	public String getIchOrgCode() {
		return ichOrgCode;
	}
	public void setIchOrgCode(String ichOrgCode) {
		this.ichOrgCode = ichOrgCode;
	}
	public String getIchOrgName() {
		return ichOrgName;
	}
	public void setIchOrgName(String ichOrgName) {
		this.ichOrgName = ichOrgName;
	}
	public Double getIchTotalAmount() {
		return ichTotalAmount;
	}
	public void setIchTotalAmount(Double ichTotalAmount) {
		this.ichTotalAmount = ichTotalAmount;
	}
	public String getIchIdNum() {
		return ichIdNum;
	}
	public void setIchIdNum(String ichIdNum) {
		this.ichIdNum = ichIdNum;
	}
	public String getIchDistrictCode() {
		return ichDistrictCode;
	}
	public void setIchDistrictCode(String ichDistrictCode) {
		this.ichDistrictCode = ichDistrictCode;
	}
	public String getIchNodeCode() {
		return ichNodeCode;
	}
	public void setIchNodeCode(String ichNodeCode) {
		this.ichNodeCode = ichNodeCode;
	}
	public String getIchDcsId() {
		return ichDcsId;
	}
	public void setIchDcsId(String ichDcsId) {
		this.ichDcsId = ichDcsId;
	}
	public String getIchDiseaseCode() {
		return ichDiseaseCode;
	}
	public void setIchDiseaseCode(String ichDiseaseCode) {
		this.ichDiseaseCode = ichDiseaseCode;
	}
	public String getIchDisease() {
		return ichDisease;
	}
	public void setIchDisease(String ichDisease) {
		this.ichDisease = ichDisease;
	}
	public Long getIchDiagnosisDate() {
		return ichDiagnosisDate;
	}
	public void setIchDiagnosisDate(Long ichDiagnosisDate) {
		this.ichDiagnosisDate = ichDiagnosisDate;
	}
	
	public String toString(){
		return "IchBean{" +
                "ichId='" + ichId + '\'' +
                ", ichOrgCode=" + ichOrgCode +
                ", ichOrgName=" + ichOrgName +
                ", ichTotalAmount='" + ichTotalAmount + '\'' +
                ", ichIdNum=" + ichIdNum +
                ", ichDistrictCode=" + ichDistrictCode +
                ", ichNodeCode=" + ichNodeCode +
                ", ichDcsId=" + ichDcsId +
                ", ichDiseaseCode=" + ichDiseaseCode +
                ", ichDisease=" + ichDisease +
                ", ichDiagnosisDate=" + ichDiagnosisDate +
                '}';
	}
    
	
}
