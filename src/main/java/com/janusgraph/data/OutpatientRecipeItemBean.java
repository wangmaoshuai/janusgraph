package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel
public class OutpatientRecipeItemBean {
	
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "recipeItemOrIdIndex") 
	private String recipeItemOrId;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "recipeItemDrugCodeIndex") 
	private String recipeItemDrugCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "recipeItemAndPerson")
	private String recipeItemDrugCommonName;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "recipeItemAndPerson")
	private String recipeItemDrugName;
	
	 public String getRecipeItemOrId() {
		return recipeItemOrId;
	}
	public void setRecipeItemOrId(String recipeItemOrId) {
		this.recipeItemOrId = recipeItemOrId;
	}
	public String getRecipeItemDrugCode() {
		return recipeItemDrugCode;
	}
	public void setRecipeItemDrugCode(String recipeItemDrugCode) {
		this.recipeItemDrugCode = recipeItemDrugCode;
	}
	public String getRecipeItemDrugCommonName() {
		return recipeItemDrugCommonName;
	}
	public void setRecipeItemDrugCommonName(String recipeItemDrugCommonName) {
		this.recipeItemDrugCommonName = recipeItemDrugCommonName;
	}
	public String getRecipeItemDrugName() {
		return recipeItemDrugName;
	}
	public void setRecipeItemDrugName(String recipeItemDrugName) {
		this.recipeItemDrugName = recipeItemDrugName;
	}
	
	public String toString() {
        return "OutpatientRecipeItemBean{" +
                "recipeItemOrId='" + recipeItemOrId + '\'' +
                ", recipeItemDrugCode=" + recipeItemDrugCode +
                ", recipeItemDrugCommonName=" + recipeItemDrugCommonName +
                ", recipeItemDrugName='" + recipeItemDrugName + '\'' +
                '}';
    }
	
	
	
	
}
