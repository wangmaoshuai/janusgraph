package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel
public class OutpatientRecipeBean {
	
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "recipeOutpatientNumIndex")
	private String recipeOutpatientNum;
	
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "recipeIdNumIndex")
	private String recipeIdNum;
	
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "prescribeDateAndDiseaseCodeIndex")
	private Long recipePrescribeDate;
	
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "outPatientRecipeAndPerson")
	private String recipeDistrictCode;
	
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "prescribeDateAndDiseaseCodeIndex")
	private String recipeDiseaseCode; 
	
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "outPatientRecipeAndPerson")
	private String recipeDisease;
	
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "recipeItemDrugCodeIndex")
	private String recipeItemDrugCode;
	
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "outPatientRecipeAndPerson")
	private String recipeItemDrugCommonName;
	
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "outPatientRecipeAndPerson")
	private String recipeItemDrugName;

	
	 public String getRecipeOutpatientNum() {
		return recipeOutpatientNum;
	}
	public void setRecipeOutpatientNum(String recipeOutpatientNum) {
		this.recipeOutpatientNum = recipeOutpatientNum;
	}
	public Long getRecipePrescribeDate() {
		return recipePrescribeDate;
	}
	public void setRecipePrescribeDate(Long recipePrescribeDate) {
		this.recipePrescribeDate = recipePrescribeDate;
	}
	public String getRecipeDistrictCode() {
		return recipeDistrictCode;
	}
	public void setRecipeDistrictCode(String recipeDistrictCode) {
		this.recipeDistrictCode = recipeDistrictCode;
	}
	public String getRecipeDiseaseCode() {
		return recipeDiseaseCode;
	}
	public void setRecipeDiseaseCode(String recipeDiseaseCode) {
		this.recipeDiseaseCode = recipeDiseaseCode;
	}
	public String getRecipeOutDisease() {
		return recipeDisease;
	}
	public void setRecipeOutDisease(String recipeOutDisease) {
		this.recipeDisease = recipeOutDisease;
	}
	public String getRecipeItemDrugCode() {
		return recipeItemDrugCode;
	}
	public void setRecipeItemDrugCode(String recipeItemDrugCode) {
		this.recipeItemDrugCode = recipeItemDrugCode;
	}
	public String getRecipeItemDrugCommonName() {
		return recipeItemDrugCommonName;
	}
	public void setRecipeItemDrugCommonName(String recipeItemDrugCommonName) {
		this.recipeItemDrugCommonName = recipeItemDrugCommonName;
	}
	public String getRecipeItemDrugName() {
		return recipeItemDrugName;
	}
	public void setRecipeItemDrugName(String recipeItemDrugName) {
		this.recipeItemDrugName = recipeItemDrugName;
	}
	public String toString() {
	        return "OutpatientRecipeBean{" +
	                "recipeOutpatientNum='" + recipeOutpatientNum + '\'' +
	                ", recipeIdNum=" + recipeIdNum +
	                ", recipePrescribeDate=" + recipePrescribeDate +
	                ", recipeDistrictCode='" + recipeDistrictCode + '\'' +
	                ", recipeDiseaseCode=" + recipeDiseaseCode +
	                ", recipeDisease=" + recipeDisease +
	                ", recipeItemDrugCode=" + recipeItemDrugCode +
	                ", recipeItemDrugCommonName=" + recipeItemDrugCommonName +
	                ", recipeItemDrugName=" + recipeItemDrugName  +
	                '}';
	    }
	public String getRecipeIdNum() {
		return recipeIdNum;
	}
	public void setRecipeIdNum(String recipeIdNum) {
		this.recipeIdNum = recipeIdNum;
	}	
}
