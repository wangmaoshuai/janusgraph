package com.janusgraph.data;

public class InvestmentEvent {
    private String eventName;
    private Long date;
    private Double amountOfMoney;
    private String financing;

    public InvestmentEvent(String eventName, Long date, Double amountOfMoney, String financing) {
        this.eventName = eventName;
        this.date = date;
        this.amountOfMoney = amountOfMoney;
        this.financing = financing;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(Double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public String getFinancing() {
        return financing;
    }

    public void setFinancing(String financing) {
        this.financing = financing;
    }

    @Override
    public String toString() {
        return "InvestmentEvent{" +
                "eventName='" + eventName + '\'' +
                ", date=" + date +
                ", amountOfMoney=" + amountOfMoney +
                ", financing='" + financing + '\'' +
                '}';
    }
}
