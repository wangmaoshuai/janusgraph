package com.janusgraph.data;

import com.janusgraph.annotation.GraphLabel;

@GraphLabel(Label = "Province")
public class Province extends City {
    public Province(String city, String phoneAreaCode, String zipCode, String administrativeCode) {
        super(city, phoneAreaCode, zipCode, administrativeCode);
    }
}
