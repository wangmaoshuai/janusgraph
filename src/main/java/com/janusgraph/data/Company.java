package com.janusgraph.data;
import com.janusgraph.annotation.*;

@GraphLabel
public class Company {
    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"companyNameIndex", "companyIndex"})
    private String companyName;
    private String companyLocation;
    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"creatorIndex", "companyIndex"})
    private String creator;

    public Company(String companyName, String companyLocation, String creator) {
        this.companyName = companyName;
        this.companyLocation = companyLocation;
        this.creator = creator;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyLocation() {
        return companyLocation;
    }

    public void setCompanyLocation(String companyLocation) {
        this.companyLocation = companyLocation;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}
