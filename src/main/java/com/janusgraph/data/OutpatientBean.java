package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel(Label="Outpatient")
public class OutpatientBean {
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "outPatientNumAndDoctorIdNumIndex")
	private String outpatientNum;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "outPatientRecordIdNumIndex")
	private String outpatientIdNum;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private String outpatientChiefComplaint;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private String outpatientMedicalHistory;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "AdminIllnessCodeAndOrgCodeIndex")
	private String outpatientIllnessCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private String outpatientIllnessName;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private Double outpatientAllCost;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "AdminIllnessCodeAndOrgCodeIndex")
	private String outpatientOrgCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private String outpatientOrgName;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "outPatientNumAndDoctorIdNumIndex")
	private String outpatientDoctorIdNum;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private String outpatientDoctorName;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
	private Long outpatientDate;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "outpatientDateIndex")
	private String outpatientDistrictCode;

	public String getOutpatientNum() {
		return outpatientNum;
	}
	public void setOutpatientNum(String outpatientNum) {
		this.outpatientNum = outpatientNum;
	}
	public String getOutPatientIdNum() {
		return outpatientIdNum;
	}
	public void setOutPatientIdNum(String outPatientIdNum) {
		this.outpatientIdNum = outPatientIdNum;
	}
	public String getOutPatientChiefComplaint() {
		return outpatientChiefComplaint;
	}
	public void setOutPatientChiefComplaint(String outPatientChiefComplaint) {
		this.outpatientChiefComplaint = outPatientChiefComplaint;
	}
	public String getOutPatientMedicalHistory() {
		return outpatientMedicalHistory;
	}
	public void setOutPatientMedicalHistory(String outPatientMedicalHistory) {
		this.outpatientMedicalHistory = outPatientMedicalHistory;
	}
	public String getOutPatientIllnessCode() {
		return outpatientIllnessCode;
	}
	public void setOutPatientIllnessCode(String outPatientIllnessCode) {
		this.outpatientIllnessCode = outPatientIllnessCode;
	}
	public String getOutPatientIllnessName() {
		return outpatientIllnessName;
	}
	public void setOutPatientIllnessName(String outPatientIllnessName) {
		this.outpatientIllnessName = outPatientIllnessName;
	}
	public Double getOutPatientAllCost() {
		return outpatientAllCost;
	}
	public void setOutPatientAllCost(Double outPatientAllCost) {
		this.outpatientAllCost = outPatientAllCost;
	}
	public String getOutPatientOrgCode() {
		return outpatientOrgCode;
	}
	public void setOutPatientOrgCode(String outPatientOrgCode) {
		this.outpatientOrgCode = outPatientOrgCode;
	}
	public String getOutPatientOrgName() {
		return outpatientOrgName;
	}
	public void setOutPatientOrgName(String outPatientOrgName) {
		this.outpatientOrgName = outPatientOrgName;
	}
	public String getOutpatientDoctorIdNum() {
		return outpatientDoctorIdNum;
	}
	public void setOutpatientDoctorIdNum(String outpatientDoctorIdNum) {
		this.outpatientDoctorIdNum = outpatientDoctorIdNum;
	}
	public String getOutpatientDoctorName() {
		return outpatientDoctorName;
	}
	public void setOutpatientDoctorName(String outpatientDoctorName) {
		this.outpatientDoctorName = outpatientDoctorName;
	}
	public Long getOutpatientDate() {
		return outpatientDate;
	}
	public void setOutpatientDate(Long outpatientDate) {
		this.outpatientDate = outpatientDate;
	}
	public String getOutPatientDistrictCode() {
		return outpatientDistrictCode;
	}
	public void setOutPatientDistrictCode(String outPatientDistrictCode) {
		this.outpatientDistrictCode = outPatientDistrictCode;
	}
	
	public String toString() {
	        return "OutpatientBean{" +
	                "outpatientNum='" + outpatientNum + '\'' +
	                ", outpatientIdNum=" + outpatientIdNum +
	                ", outpatientChiefComplaint=" + outpatientChiefComplaint  +
	                ", outpatientMedicalHistory='" + outpatientMedicalHistory + '\'' +
	                ", outpatientIllnessCode=" + outpatientIllnessCode +
	                ", outpatientIllnessName=" + outpatientIllnessName +
	                ", outpatientAllCost=" + outpatientAllCost +
	                ", outpatientOrgCode=" + outpatientOrgCode +
	                ", outpatientOrgName=" + outpatientOrgName +
	                ", outpatientDoctorIdNum=" + outpatientDoctorIdNum +
	                ", outpatientDoctorName=" + outpatientDoctorName +
	                ", outpatientDate=" + outpatientDate +
	                ", outpatientDistrictCode=" + outpatientDistrictCode +
	                '}';
	    }
}
