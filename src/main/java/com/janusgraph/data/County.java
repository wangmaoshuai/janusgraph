package com.janusgraph.data;

import com.janusgraph.annotation.GraphLabel;

@GraphLabel(Label = "County")
public class County extends City {
    public County(String city, String phoneAreaCode, String zipCode, String administrativeCode) {
        super(city, phoneAreaCode, zipCode, administrativeCode);
    }
}
