package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;
@GraphLabel(Label = "City")
public class City {
    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"cityNameIndex", "cityIndex"})
    public final String city;
    @GraphIndex(IndexType = {IndexType.MIXED}, IndexName = {"cityIndex"})
    public final String phoneAreaCode;
    @GraphIndex(IndexType = {IndexType.MIXED}, IndexName = {"cityIndex"})
    public final String zipCode;
    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"administrativeCodeIndex", "cityIndex"})
    public final String administrativeCode;

    public City(String city, String phoneAreaCode, String zipCode, String administrativeCode) {
        this.city = city;
        this.administrativeCode = administrativeCode;
        this.phoneAreaCode = phoneAreaCode;
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "City{" +
                "city='" + city + '\'' +
                ", phoneAreaCode='" + phoneAreaCode + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", administrativeCode='" + administrativeCode + '\'' +
                '}';
    }
}
