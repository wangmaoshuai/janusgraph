package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel
public final class Disease {
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "diseaseIndex")
    public final String diseaseName;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "diseaseIndex")
    public final String ICD10Code;

    public Disease(String diseaseName, String ICD10Code) {
        this.diseaseName = diseaseName;
        this.ICD10Code = ICD10Code;
    }
}
