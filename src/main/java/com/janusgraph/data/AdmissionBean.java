package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;
import org.apache.ibatis.type.Alias;

/**
 * Created by xieshiling on 2017/10/25.
 */
@Alias("AdmissionBean")
@GraphLabel
public final class AdmissionBean { 

	private String admissionId;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "OrgCodeAndAdmDiseaseCodeAndAdmDateIndex")
    private String admissionOrgCode;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "OrgCodeAndAdmDiseaseCodeAndAdmDateIndex")
    private String admissionOrgName;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "admissionIdNumIndex")
    private String admissionIdNum;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "OrgCodeAndAdmDiseaseCodeAndAdmDateIndex")
    private Long admissionDate;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionChiefComplaint;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionPresentIllness;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionPastHistory;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionPersonalHistory;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionFamilyHistory;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "OrgCodeAndAdmDiseaseCodeAndAdmDateIndex")
    private String admissionDoctorName;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "OrgCodeAndAdmDiseaseCodeAndAdmDateIndex")
    private String admissionDoctorCode;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionDistrictCode;
    @GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "OrgCodeAndAdmDiseaseCodeAndAdmDateIndex")
    private String admissionDiseaseCode;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionDisease;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private String admissionDiagnosisBasis;
    @GraphIndex(IndexType = IndexType.MIXED, IndexName = "admissionAndPerson")
    private Long admissionDiagnosisDate;

    
    public String getAdmissionId() {
		return admissionId;
	}
	public void setAdmissionId(String admissionId) {
		this.admissionId = admissionId;
	}
	public String getAdmissionOrgCode() {
		return admissionOrgCode;
	}
	public void setAdmissionOrgCode(String admissionOrgCode) {
		this.admissionOrgCode = admissionOrgCode;
	}
	public String getAdmissionOrgName() {
		return admissionOrgName;
	}
	public void setAdmissionOrgName(String admissionOrgName) {
		this.admissionOrgName = admissionOrgName;
	}
	public String getAdmissionIdNum() {
		return admissionIdNum;
	}
	public void setAdmissionIdNum(String admissionIdNum) {
		this.admissionIdNum = admissionIdNum;
	}
	public Long getAdmissionDate() {
		return admissionDate;
	}
	public void setAdmissionDate(Long admissionDate) {
		this.admissionDate = admissionDate;
	}
	public String getAdmissionchiefComplaint() {
		return admissionChiefComplaint;
	}
	public void setAdmissionchiefComplaint(String admissionchiefComplaint) {
		this.admissionChiefComplaint = admissionchiefComplaint;
	}
	public String getAdmissionPresentIllness() {
		return admissionPresentIllness;
	}
	public void setAdmissionPresentIllness(String admissionPresentIllness) {
		this.admissionPresentIllness = admissionPresentIllness;
	}
	public String getAdmissionPastHistory() {
		return admissionPastHistory;
	}
	public void setAdmissionPastHistory(String admissionPastHistory) {
		this.admissionPastHistory = admissionPastHistory;
	}
	public String getAdmissionPersonalHistory() {
		return admissionPersonalHistory;
	}
	public void setAdmissionPersonalHistory(String admissionPersonalHistory) {
		this.admissionPersonalHistory = admissionPersonalHistory;
	}
	public String getAdmissionFamilyHistory() {
		return admissionFamilyHistory;
	}
	public void setAdmissionFamilyHistory(String admissionFamilyHistory) {
		this.admissionFamilyHistory = admissionFamilyHistory;
	}
	public String getAdmissionDoctorName() {
		return admissionDoctorName;
	}
	public void setAdmissionDoctorName(String admissionDoctorName) {
		this.admissionDoctorName = admissionDoctorName;
	}
	public String getAdmissionDoctorCode() {
		return admissionDoctorCode;
	}
	public void setAdmissionDoctorCode(String admissionDoctorCode) {
		this.admissionDoctorCode = admissionDoctorCode;
	}
	public String getAdmissionDistrictCode() {
		return admissionDistrictCode;
	}
	public void setAdmissionDistrictCode(String admissionDistrictCode) {
		this.admissionDistrictCode = admissionDistrictCode;
	}
	public String getAdmissionDiseaseCode() {
		return admissionDiseaseCode;
	}
	public void setAdmissionDiseaseCode(String admissionDiseaseCode) {
		this.admissionDiseaseCode = admissionDiseaseCode;
	}
	public String getAdmissionDisease() {
		return admissionDisease;
	}
	public void setAdmissionDisease(String admissionDisease) {
		this.admissionDisease = admissionDisease;
	}
	public String getAdmissionDiagnosisBasis() {
		return admissionDiagnosisBasis;
	}
	public void setAdmissionDiagnosisBasis(String admissionDiagnosisBasis) {
		this.admissionDiagnosisBasis = admissionDiagnosisBasis;
	}
	public Long getAdmissionDiagnosisDate() {
		return admissionDiagnosisDate;
	}
	public void setAdmissionDiagnosisDate(Long admissionDiagnosisDate) {
		this.admissionDiagnosisDate = admissionDiagnosisDate;
	}
	public String toString() {
        return "AdmissionBean{" +
                "admissionOrgCode='" + admissionOrgCode + '\'' +
                ", admissionOrgName=" + admissionOrgName +
                ", admissionId=" + admissionId +
                ", admissionIdNum='" + admissionIdNum + '\'' +
                ", admissionDate=" + admissionDate +
                ", admissionChiefComplaint=" + admissionChiefComplaint +
                ", admissionPresentIllness=" + admissionPresentIllness +
                ", admissionPastHistory=" + admissionPastHistory +
                ", admissionPersonalHistory=" + admissionPersonalHistory +
                ", admissionFamilyHistory=" + admissionFamilyHistory +
                ", admissionDoctorName=" + admissionDoctorName +
                ", admissionDoctorCode=" + admissionDoctorCode +
                ", admissionDistrictCode=" + admissionDistrictCode +
                ", admissionDiseaseCode=" + admissionDiseaseCode +
                ", admissionDisease=" + admissionDisease +
                ", admissionDiagnosisBasis=" + admissionDiagnosisBasis +
                ", admissionDiagnosisDate=" + admissionDiagnosisDate +
                '}';
    }

}
