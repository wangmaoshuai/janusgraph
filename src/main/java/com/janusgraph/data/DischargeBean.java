package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel
public class DischargeBean {
	
	private String dischargeAdmissionId;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "dischargeIdNumIndex")
	private String dischargeIdNum;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "doctorCodeAndDiseaseCodeAndChargeDateIndex")
	private Long dischargeDate;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private Double ichTotalAmount; 
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private String dischargeAdmSituation;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private String dischargeTreatProcess;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private String dischargeSituation;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "doctorCodeAndDiseaseCodeAndChargeDateIndex")
	private String dischargeDoctorCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private String dischargeDoctorName;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private String dischargeDistrictCode;
	@GraphIndex(IndexType = IndexType.COMPOSITE, IndexName = "doctorCodeAndDiseaseCodeAndChargeDateIndex")
	private String dischargeDiseaseCode;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private String dischargeDisease;
	@GraphIndex(IndexType = IndexType.MIXED, IndexName = "dischargeAndPerson")
	private Long dischargeConfirmedDate;
	public String getDischargeAdmissionId() {
		return dischargeAdmissionId;
	}
	public void setDischargeAdmissionId(String dischargeAdmissionId) {
		this.dischargeAdmissionId = dischargeAdmissionId;
	}
	public String getDischargeIdNum() {
		return dischargeIdNum;
	}
	public void setDischargeIdNum(String dischargeIdNum) {
		this.dischargeIdNum = dischargeIdNum;
	}
	public Long getDischargeDate() {
		return dischargeDate;
	}
	public void setDischargeDate(Long dischargeDate) {
		this.dischargeDate = dischargeDate;
	}
	public Double getIchTotalAmount() {
		return ichTotalAmount;
	}
	public void setIchTotalAmount(Double ichTotalAmount) {
		this.ichTotalAmount = ichTotalAmount;
	}
	public String getDischargeAdmSituation() {
		return dischargeAdmSituation;
	}
	public void setDischargeAdmSituation(String dischargeAdmSituation) {
		this.dischargeAdmSituation = dischargeAdmSituation;
	}
	public String getDischargeTreatProcess() {
		return dischargeTreatProcess;
	}
	public void setDischargeTreatProcess(String dischargeTreatProcess) {
		this.dischargeTreatProcess = dischargeTreatProcess;
	}
	public String getDischargeSituation() {
		return dischargeSituation;
	}
	public void setDischargeSituation(String dischargeSituation) {
		this.dischargeSituation = dischargeSituation;
	}
	public String getDischargeDoctorCode() {
		return dischargeDoctorCode;
	}
	public void setDischargeDoctorCode(String dischargeDoctorCode) {
		this.dischargeDoctorCode = dischargeDoctorCode;
	}
	public String getDischargeDoctorName() {
		return dischargeDoctorName;
	}
	public void setDischargeDoctorName(String dischargeDoctorName) {
		this.dischargeDoctorName = dischargeDoctorName;
	}
	public String getDischargeDistrictCode() {
		return dischargeDistrictCode;
	}
	public void setDischargeDistrictCode(String dischargeDistrictCode) {
		this.dischargeDistrictCode = dischargeDistrictCode;
	}
	public String getDischargeDiseaseCode() {
		return dischargeDiseaseCode;
	}
	public void setDischargeDiseaseCode(String dischargeDiseaseCode) {
		this.dischargeDiseaseCode = dischargeDiseaseCode;
	}
	public String getDischargeDisease() {
		return dischargeDisease;
	}
	public void setDischargeDisease(String dischargeDisease) {
		this.dischargeDisease = dischargeDisease;
	}
	public Long getDischargeConfirmedDate() {
		return dischargeConfirmedDate;
	}
	public void setDischargeConfirmedDate(Long dischargeConfirmedDate) {
		this.dischargeConfirmedDate = dischargeConfirmedDate;
	}
	
	public String toString(){
		return "DischargeBean{"+
	"dischargeId =" + dischargeAdmissionId +'\'' +
	",dischargeOrgCode=" + dischargeIdNum +
	",dischargeOrgName=" + dischargeDate +
	"dischargeIdNum=" + ichTotalAmount + '\'' +
	",dischargeDate=" + dischargeAdmSituation +
	",dischargeAdmSituation=" + dischargeTreatProcess +
	",dischargeTreatProcess=" + dischargeSituation +
	",dischargeSituation=" + dischargeDoctorCode +
	",dischargeDistrictCode=" + dischargeDoctorName +
	",dischargeNodeCode=" + dischargeDistrictCode+
	",dischargeDrId=" + dischargeDiseaseCode +
	",dischargeDiagnosisDiseaseCode=" + dischargeDisease +
	",dischargeDiagnosisDisease=" + dischargeConfirmedDate +
	'}';
	}
	
}
