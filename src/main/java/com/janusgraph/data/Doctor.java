package com.janusgraph.data;

import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.IndexType;

@GraphLabel
public final class Doctor {

    @GraphIndex(IndexType = {IndexType.COMPOSITE, IndexType.MIXED}, IndexName = {"doctorNameIndex", "doctorIndex"})
    public final String doctorName;
    @GraphIndex(IndexType = {IndexType.UNIQUENESS, IndexType.MIXED}, IndexName = {"doctorIdIndex", "doctorIndex"})
    public final String doctorCardId;
    public final String doctorCode;
    public final String doctorOrgCode;
    public final String physicianLevel;

    public Doctor(String doctorName, String doctorCardId, String doctorOrgCode, String physicianLevel, String doctorCode) {
        this.doctorName = doctorName;
        this.doctorCardId = doctorCardId;
        this.doctorOrgCode = doctorOrgCode;
        this.physicianLevel = physicianLevel;
        this.doctorCode = doctorCode;
    }
}
