package com.janusgraph.schema;

/**
 * Created by Diablo on 2017/9/26.
 * janusGraph schema manager
 */
public interface SchemaManager<T, S> {
    T createSchema(S source);
}
