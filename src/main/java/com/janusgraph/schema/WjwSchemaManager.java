package com.janusgraph.schema;

/**
 * Created by shaozhuquan on 2017/10/24.
 */
public interface WjwSchemaManager<T,S> {

    T createPersonalSchema(S source);

    T createOrgSchema(S source);

    T createAdmissionSchema(S source);

    T createAdmissionExamSchema(S source);

    T createDischargeSchema(S source);

    T createIchSchema(S source);

    T createOutpatientRecipeSchema(S source);

    T createOutpatientRecipeItemSchema(S source);

    T createOutpatientSchema(S source);
}
