package com.janusgraph.schema;


/**
 * Created by Diablo on 2017/9/26.
 * 创建处理天气的schemal
 */
public class RequestSchemaManager implements SchemaManager<String, String> {

    @Override
    public String createSchema(String source) {
        final StringBuilder s = new StringBuilder();
        s.append("JanusGraphManagement mgmt = graph.openManagement(); ");
        s.append("boolean created = false; ");
        s.append(
                "if (mgmt.getRelationTypes(RelationType.class).iterator().hasNext()) { mgmt.rollback(); created = false; } else { ");
        // properties
        s.append("PropertyKey City = mgmt.makePropertyKey(\"City\").dataType(String.class).make(); ");
        s.append("PropertyKey date = mgmt.makePropertyKey(\"date\").dataType(Date.class).make(); ");
        s.append("PropertyKey pm25 = mgmt.makePropertyKey(\"pm25\").dataType(Integer.class).make(); ");
        s.append("PropertyKey qualityGrade = mgmt.makePropertyKey(\"qualityGrade\").dataType(String.class).make(); ");
        s.append("PropertyKey AQI = mgmt.makePropertyKey(\"AQI\").dataType(Integer.class).make(); ");
        s.append("PropertyKey AQIRank = mgmt.makePropertyKey(\"AQIRank\").dataType(Integer.class).make(); ");
        s.append("PropertyKey pm10 = mgmt.makePropertyKey(\"pm10\").dataType(Integer.class).make(); ");
        s.append("PropertyKey So2 = mgmt.makePropertyKey(\"So2\").dataType(Integer.class).make(); ");
        s.append("PropertyKey No2 = mgmt.makePropertyKey(\"No2\").dataType(Integer.class).make(); ");
        s.append("PropertyKey Co = mgmt.makePropertyKey(\"Co\").dataType(Double.class).make(); ");
        s.append("PropertyKey O3 = mgmt.makePropertyKey(\"O3\").dataType(Integer.class).make(); ");

        // label
        s.append("mgmt.makeVertexLabel(\"pm25\").make(); ");
        s.append("mgmt.buildIndex(\"qualityGradeIndex\", Vertex.class).addKey(qualityGrade).buildCompositeIndex(); ");
        s.append("mgmt.buildIndex(\"cityIndex\", Vertex.class).addKey(City).buildCompositeIndex(); ");
        s.append("mgmt.buildIndex(\"weatherAndPerson\", Vertex.class).addKey(City,Mapping.STRING.asParameter()).addKey(pm25).addKey(date)" +
                ".addKey(AQI).addKey(AQIRank).addKey(qualityGrade,Mapping.STRING.asParameter()).addKey(pm10).addKey(So2).addKey(No2).addKey(Co).addKey(O3).buildMixedIndex(\"search\");");
        s.append("mgmt.commit(); created = true; }");

        return s.toString();
    }
}
