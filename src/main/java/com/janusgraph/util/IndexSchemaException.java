package com.janusgraph.util;

/**
 * Created by Diablo on 2017/10/25.
 */
public class IndexSchemaException extends Exception {
    public IndexSchemaException() {
        super();
    }

    public IndexSchemaException(String s) {
        super(s);
    }
}
