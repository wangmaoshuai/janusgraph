package com.janusgraph.util;

import org.apache.log4j.Logger;

/**
 * Created by shaozhuquan on 2017/11/2.
 */
public abstract class CreateEdge {
    private static final Logger LOGGER = Logger.getLogger(CreateEdge.class);

    public abstract void addE(String label1,String label2,String[] compareColumns1,String[] compareColumns2,String EdgeLabel);
}
