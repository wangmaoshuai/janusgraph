package com.janusgraph.util;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.janusgraph.annotation.GraphIndex;
import com.janusgraph.annotation.GraphLabel;
import com.janusgraph.annotation.GraphProperty;
import com.janusgraph.annotation.IndexType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.schema.JanusGraphIndex;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.core.schema.Mapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Diablo on 2017/10/25.
 * Graph 基础操作的类。以后会改名
 */
public class GraphHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphHandler.class);
    private static final String LABEL = "label";

    private final GraphTraversalSource g;

    public GraphHandler(GraphTraversalSource g) {
        this.g = g;
    }

    public void addV(Object o) throws IllegalAccessException {
        addV(o, true);
    }

    public void addV(Object o, boolean autoCommit) throws IllegalAccessException {
        final Bindings b = new Bindings();
        String label = getLabel(o.getClass()).get();
        List<Pair> propertiesData = getPropertyData(o);
        GraphTraversal<Vertex, Vertex> v = g.addV(b.of(LABEL, label));
        propertiesData.stream().filter(x -> x.getValue() != null).forEach(x -> v.property(x.getKey(), b.of(x.getKey().toString(), x.getValue())));
        v.next();
        if (!autoCommit) {
            g.tx().commit();
        }
        LOGGER.info("Add V label {}", label);
        LOGGER.info("Add V property {}", propertiesData.toString());
    }

    private Optional<String> getLabel(Class<?> o) {
        Annotation[] annotations = o.getAnnotations();
        GraphLabel labelAnno = (GraphLabel) Arrays.stream(annotations).filter(a -> a instanceof GraphLabel).collect(Collectors.toList()).get(0);
        String label = labelAnno.Label();
        if (StringUtils.isEmpty(label) && StringUtils.isBlank(label)) {
            label = o.getSimpleName();
        }
        return Optional.of(label);
    }

    private List<Pair> getPropertyData(Object o) throws IllegalAccessException {
        List<Pair> v = new ArrayList<>();
        Field[] allFields = FieldUtils.getAllFields(o.getClass());
        for (Field f : allFields) {
            if (Modifier.isPrivate(f.getModifiers())) {
                f.setAccessible(true);
            }
            String property;
            GraphProperty annotation = f.getAnnotation(GraphProperty.class);
            if (annotation != null) {
                property = annotation.Property();
                if (StringUtils.isEmpty(property)) {
                    property = f.getName();
                }
            } else {
                property = f.getName();
            }
            v.add(new ImmutablePair(property, f.get(o)));
        }
        return v;
    }

    public void createVSchema(Class<?> c, String backingIndex) throws IndexSchemaException {
        JanusGraphManagement mgmt = ((JanusGraph) g.getGraph()).openManagement();
        Table<IndexType, String, List<SchemaInfo>> infos = getSchema(c);
        String label = getLabel(c).get();
        Set<SchemaInfo> properties = new HashSet<>();
        try {
            mgmt.makeVertexLabel(label);
            Set<Table.Cell<IndexType, String, List<SchemaInfo>>> allPorperty = infos.cellSet();
            allPorperty.forEach(x -> {
                List<SchemaInfo> schemaInfoList = x.getValue();
                properties.addAll(schemaInfoList);
            });
            properties.stream().filter(x -> mgmt.getPropertyKey(x.propertyName) == null).forEach(p -> mgmt.makePropertyKey(p.propertyName).dataType(p.propertyType).make());
            Set<String> columnSet = infos.columnKeySet();
            for (String column : columnSet) {
                Map<IndexType, List<SchemaInfo>> listMap = infos.column(column);
                listMap.forEach((k, v) -> {
                    JanusGraphIndex indexName = mgmt.getGraphIndex(column);
                    if (indexName == null && !k.equals(IndexType.NONE)) {
                        JanusGraphManagement.IndexBuilder tmp = mgmt.buildIndex(column, Vertex.class);
                        switch (k) {
                            case MIXED:
                                v.forEach(property -> tmp.addKey(mgmt.getPropertyKey(property.propertyName), property.dataMapping.asParameter()));
                                tmp.buildMixedIndex(backingIndex);
                                break;
                            case COMPOSITE:
                                v.forEach(property -> tmp.addKey(mgmt.getPropertyKey(property.propertyName)));
                                tmp.buildCompositeIndex();
                                break;
                            case UNIQUENESS:
                                v.forEach(property -> tmp.addKey(mgmt.getPropertyKey(property.propertyName)));
                                tmp.unique().buildCompositeIndex();
                                break;
                            case NONE:
                                break;
                        }
                    }
                });
            }
            mgmt.commit();
        } catch (Exception e) {
            mgmt.rollback();
            e.printStackTrace();
        }
    }


    private static Table<IndexType, String, List<SchemaInfo>> getSchema(Class<?> c) throws IndexSchemaException {
        Table<IndexType, String, List<SchemaInfo>> schemaTable = HashBasedTable.create();
        schemaTable.put(IndexType.NONE, "none", new ArrayList<>());
        Field[] allFields = FieldUtils.getAllFields(c);
        for (Field f : allFields) {
            if (Modifier.isPrivate(f.getModifiers())) {
                f.setAccessible(true);
            }
            GraphIndex annotation = f.getAnnotation(GraphIndex.class);
            if (annotation != null) {
                String[] indexName = annotation.IndexName();
                IndexType[] indexType = annotation.IndexType();
                if (indexName.length != indexType.length) {
                    throw new IndexSchemaException("IndexName length is not equals to IndexType length");
                }
                Class<?> propertyType = f.getType();
                String propertyName = annotation.PropertyName();
                if (StringUtils.isEmpty(propertyName)) propertyName = f.getName();
                Mapping dataMapping = annotation.Mapping();
                SchemaInfo schemaInfo = new SchemaInfo(propertyType, propertyName, dataMapping);
                if (indexName.length != 0 & indexType.length != 0) {
                    for (int i = 0; i < indexName.length; i++) {
                        if (schemaTable.contains(indexType[i], indexName[i])) {
                            schemaTable.get(indexType[i], indexName[i]).add(schemaInfo);
                        } else {
                            schemaTable.put(indexType[i], indexName[i], new ArrayList<>());
                            schemaTable.get(indexType[i], indexName[i]).add(schemaInfo);
                        }
                    }
                } else {
                    schemaTable.get(IndexType.NONE, "none").add(schemaInfo);
                }
            }

        }
        return schemaTable;
    }

    private static class SchemaInfo {
        final Class<?> propertyType;
        final String propertyName;
        final Mapping dataMapping;

        SchemaInfo(Class<?> propertyType, String propertyName, Mapping dataMapping) {
            this.dataMapping = dataMapping;
            this.propertyType = propertyType;
            this.propertyName = propertyName;
        }

    }
}