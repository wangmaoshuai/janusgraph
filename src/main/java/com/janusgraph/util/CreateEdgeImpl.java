package com.janusgraph.util;

import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by shaozhuquan on 2017/11/2.
 */
public class CreateEdgeImpl extends CreateEdge{
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateEdgeImpl.class);

    private final GraphTraversalSource g;

    private static final String OUT_V = "outV";
    private static final String IN_V = "inV";
    private static final String LABEL = "label";

    public CreateEdgeImpl(GraphTraversalSource g) {
        this.g = g;
    }

    public void addE(String label1,String label2,String[] compareColumns1,
                     String[] compareColumns2,String EdgeLabel){
        final Bindings b = new Bindings();
        List<Vertex> v1List = g.V().hasLabel(label1).toList();
        List<Vertex> v2List = g.V().hasLabel(label2).toList();
        int compareColumnLength=compareColumns1.length;
        String[] compareValue1=new String[compareColumnLength];
        String[] compareValue2=new String[compareColumnLength];
        for(Vertex v1:v1List){
            for(int i=0;i<compareColumnLength;i++){
                compareValue1[i]=g.V(v1).properties(compareColumns1[i]).next().value().toString();
            }
            for(Vertex v2:v2List){
                for(int i=0;i<compareColumnLength;i++){
                    compareValue2[i]=g.V(v2).properties(compareColumns2[i]).next().value().toString();
                }
                if(Arrays.equals(compareValue1,compareValue2)){
                    g.V(b.of(OUT_V, v1)).as("a").V(b.of(IN_V, v2)).addE(b.of(LABEL, EdgeLabel)).from("a").next();
                }
            }
        }
    }
}
