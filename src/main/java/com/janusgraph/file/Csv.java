package com.janusgraph.file;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;

import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Diablo on 2017/10/20.
 * 用于做csv关系发现并把结果输出。
 */
public final class Csv {
    private Table<Integer, Integer, Optional<String>> table;
    private List<String> columns = new ArrayList<>();
    private final int compareColumn;
    private final int pk;

    public Csv(Reader in, String compareColum, String pk) throws IOException {
        List<CSVRecord> lines = CSVFormat.EXCEL.parse(in).getRecords();
        for (String s1 : lines.get(0)) {
            this.columns.add(s1);
        }
        this.columns = this.columns.stream().map(String::toLowerCase).collect(Collectors.toList());
        this.compareColumn = columns.indexOf(compareColum.toLowerCase());
        this.pk = columns.indexOf(pk.toLowerCase());
        lines.remove(0);
        this.table = HashBasedTable.create();
        int lineNum = 0;
        for (CSVRecord lineData : lines) {
            for (int i = 0; i < lineData.size(); i++) {
                String cell = lineData.get(i);
                if (StringUtils.isEmpty(cell) && StringUtils.isBlank(cell)) {
                    this.table.put(lineNum, i, Optional.empty());
                } else {
                    this.table.put(lineNum, i, Optional.of(cell));
                }
            }
            lineNum += 1;
        }
    }

    public List<Triple<String, String, String>> getRelation(Csv compareCsv, String label) {
        List<Triple<String, String, String>> result = new ArrayList<>();
        Map<Integer, Optional<String>> compareData1 = this.table.column(compareColumn);
        Map<Integer, Optional<String>> compareData2 = compareCsv.table.column(compareCsv.compareColumn);
        compareData1.forEach((k1, v1) -> compareData2.forEach((k2, v2) -> {
            if (v1.isPresent() && v2.isPresent()) {
                if (v1.get().equals(v2.get())) {
                    String pk1Value = this.table.get(k1, this.pk).get();
                    String pk2Value = compareCsv.table.get(k2, compareCsv.pk).get();
                    result.add(new ImmutableTriple<>(pk1Value, label, pk2Value));
                }
            }
        }));
        return result;
    }

    public Csv handleCsv(String... args) {
        Table<Integer, Integer, Optional<String>> table = HashBasedTable.create();
        List<Integer> columnIds = new ArrayList<>();
        Arrays.stream(args).forEach((arg) -> columnIds.add(this.columns.indexOf(arg.toLowerCase())));
        columnIds.forEach((id) -> {
            Map<Integer, Optional<String>> columnData = this.table.column(id);
            columnData.forEach((k, v) -> table.put(k, id, v));
        });
        this.table = table;
        return this;
    }

    public Map<Integer, Optional<String>> getRow(int rowId) {
        return this.table.row(rowId);
    }

    public Map<Integer, Optional<String>> findRowByPK(String pkData) {
        Map<Integer, Optional<String>> pkColumn = this.table.column(this.pk);
        final Integer[] rowKey = new Integer[1];
        pkColumn.forEach((k, v) -> {
            if (v.get().equals(pkData)) {
                rowKey[0] = k;
            }
        });
        return this.table.row(rowKey[0]);
    }
}
