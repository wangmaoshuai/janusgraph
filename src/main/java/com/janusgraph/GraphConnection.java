package com.janusgraph;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphFactory;

import java.util.Optional;

/**
 * Created by Diablo on 2017/10/24.
 * 图数据库连接的抽象类
 */
public abstract class GraphConnection {
    private static final Logger LOGGER = Logger.getLogger(GraphConnection.class);

    protected final String propFileName;
    protected Graph graph;
    protected GraphTraversalSource g;

    public GraphConnection(final String propFileName) {
        this.propFileName = propFileName;
    }

    /**
     * 开启一个数据库连接
     *
     * @return GraphTraversalSource
     */
    public abstract GraphTraversalSource openGraph() throws ConfigurationException;

    public abstract void closeGraph() throws Exception;

    public Optional<JanusGraph> getGraph() {
        if (graph != null) {
            return Optional.of((JanusGraph) graph);
        }
        return Optional.empty();
    }

    public void dropGraph() throws Exception {
        if (getGraph().isPresent()) {
            JanusGraphFactory.drop(getGraph().get());
        }
    }
}
