package com.janusgraph.annotation;

/**
 * Created by Diablo on 2017/10/25.
 */
public enum IndexType {
    COMPOSITE("composite"),
    UNIQUENESS("uniqueness"),
    MIXED("mixed"),
    NONE("none");

    private String Type;

    IndexType(String Type) {
        this.Type = Type;
    }

    public String getType() {
        return this.Type;
    }

}
