package com.janusgraph.annotation;

import org.janusgraph.core.schema.Mapping;

import java.lang.annotation.*;

/**
 * Created by Diablo on 2017/10/25.
 * 图数据库的不同索引
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface GraphIndex {

    IndexType[] IndexType();

    String[] IndexName();

    Mapping Mapping() default Mapping.DEFAULT;

    String PropertyName() default "";
}
