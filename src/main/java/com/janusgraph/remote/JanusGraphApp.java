package com.janusgraph.remote;

import com.janusgraph.GraphConnection;
import com.janusgraph.GraphRemoteConnection;
import com.janusgraph.schema.RequestSchemaManager;
import com.janusgraph.schema.SchemaManager;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.driver.ResultSet;
import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.util.empty.EmptyGraph;
import org.janusgraph.core.RelationType;
import org.janusgraph.core.attribute.Geoshape;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.diskstorage.configuration.backend.CommonsConfiguration;
import org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration;
import org.janusgraph.graphdb.database.StandardJanusGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static org.apache.tinkerpop.gremlin.process.traversal.Order.decr;
import static org.apache.tinkerpop.gremlin.process.traversal.P.eq;
import static org.apache.tinkerpop.gremlin.process.traversal.P.gt;

/**
 * Hello world!
 */
public class JanusGraphApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(JanusGraphApp.class);

    // used for bindings
    private static final String NAME = "name";
    private static final String AGE = "age";
    private static final String TIME = "time";
    private static final String REASON = "reason";
    private static final String PLACE = "place";
    private static final String OUT_V = "outV";
    private static final String IN_V = "inV";

    private static final String LABEL = "label";
    private GraphRemoteConnection graphConnection;
    private String propFileName;
    private Configuration conf;
    private Graph graph;
    private GraphTraversalSource g;
    private boolean supportsTransactions;
    private boolean supportsGeoshape;

    private Cluster cluster;
    private Client client;

    private enum PM25 {
        city("City"),
        date("date"),
        pm25("pm25"),
        qualityGrade("qualityGrade"),
        AQI("AQI"),
        AQIRank("AQIRank"),
        pm10("pm10"),
        So2("So2"),
        No2("No2"),
        Co("Co"),
        O3("O3");
        private String name;

        PM25(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }

    public JanusGraphApp(final String propFileName) {
        this.propFileName = propFileName;
        this.supportsTransactions = false;
        this.supportsGeoshape = true;
        graphConnection = new GraphRemoteConnection(propFileName);
    }

    public GraphTraversalSource openGraph() throws ConfigurationException {
        LOGGER.info("opening graph");
        g = graphConnection.openGraph();
        client = graphConnection.getClient();
        return g;
    }

    public void closeGraph() throws Exception {
        LOGGER.info("closing graph");
        graphConnection.closeGraph();
    }

    public void dropGraph() throws Exception {
        graphConnection.dropGraph();
    }

    private void createSchema(SchemaManager schemaManager) {
        LOGGER.info("creating schema");
        //由于使用了RequestSchemaManager 所以不需要传入source，这块代码需要修改
        final String req = (String) schemaManager.createSchema(null);
        final ResultSet resultSet = client.submit(req);
        Stream<Result> futureList = resultSet.stream();
        futureList.map(result -> result.toString()).forEach(LOGGER::info);
    }

    public void createElements(String lineData) throws ParseException {
        final Bindings b = new Bindings();
        final String[] datagram = lineData.split(",");
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        g.addV(b.of(LABEL, "PM25"))
                .property(PM25.city.getName(), b.of(PM25.city.getName(), datagram[0]))
                .property(PM25.date.getName(), b.of(PM25.date.getName(), sdf.parse(datagram[1])))
                .property(PM25.pm25.getName(), b.of(PM25.pm25.getName(), Integer.valueOf(datagram[2])))
                .property(PM25.qualityGrade.getName(), b.of(PM25.qualityGrade.getName(), datagram[3]))
                .property(PM25.AQI.getName(), b.of(PM25.AQI.getName(), Integer.valueOf(datagram[4])))
                .property(PM25.AQIRank.getName(), b.of(PM25.AQIRank.getName(), Integer.valueOf(datagram[5])))
                .property(PM25.pm10.getName(), b.of(PM25.pm10.getName(), Integer.valueOf(datagram[6])))
                .property(PM25.So2.getName(), b.of(PM25.So2.getName(), Integer.valueOf(datagram[7])))
                .property(PM25.No2.getName(), b.of(PM25.No2.getName(), Integer.valueOf(datagram[8])))
                .property(PM25.Co.getName(), b.of(PM25.Co.getName(), Double.valueOf(datagram[9])))
                .property(PM25.O3.getName(), b.of(PM25.O3.getName(), Integer.valueOf(datagram[10])))
                .next();
    }

    public void addElementForTest() {
        LOGGER.info("creating elements");

        final Bindings b = new Bindings();

        // see GraphOfTheGodsFactory.java

        Vertex saturn = g.addV(b.of(LABEL, "titan")).property(NAME, b.of(NAME, "saturn"))
                .property(AGE, b.of(AGE, 10000)).next();
        Vertex sky = g.addV(b.of(LABEL, "location")).property(NAME, b.of(NAME, "sky")).next();
        Vertex sea = g.addV(b.of(LABEL, "location")).property(NAME, b.of(NAME, "sea")).next();
        Vertex jupiter = g.addV(b.of(LABEL, "god")).property(NAME, b.of(NAME, "jupiter")).property(AGE, b.of(AGE, 5000))
                .next();
        Vertex neptune = g.addV(b.of(LABEL, "god")).property(NAME, b.of(NAME, "neptune")).property(AGE, b.of(AGE, 4500))
                .next();
        Vertex hercules = g.addV(b.of(LABEL, "demigod")).property(NAME, b.of(NAME, "hercules"))
                .property(AGE, b.of(AGE, 30)).next();
        Vertex alcmene = g.addV(b.of(LABEL, "human")).property(NAME, b.of(NAME, "alcmene")).property(AGE, b.of(AGE, 45))
                .next();
        Vertex pluto = g.addV(b.of(LABEL, "god")).property(NAME, b.of(NAME, "pluto")).property(AGE, b.of(AGE, 4000))
                .next();
        Vertex nemean = g.addV(b.of(LABEL, "monster")).property(NAME, b.of(NAME, "nemean")).next();
        Vertex hydra = g.addV(b.of(LABEL, "monster")).property(NAME, b.of(NAME, "hydra")).next();
        Vertex cerberus = g.addV(b.of(LABEL, "monster")).property(NAME, b.of(NAME, "cerberus")).next();
        Vertex tartarus = g.addV(b.of(LABEL, "location")).property(NAME, b.of(NAME, "tartarus")).next();

        g.V(b.of(OUT_V, jupiter)).as("a").V(b.of(IN_V, saturn)).addE(b.of(LABEL, "father")).from("a").next();
        g.V(b.of(OUT_V, jupiter)).as("a").V(b.of(IN_V, sky)).addE(b.of(LABEL, "lives"))
                .property(REASON, b.of(REASON, "loves fresh breezes")).from("a").next();
        g.V(b.of(OUT_V, jupiter)).as("a").V(b.of(IN_V, neptune)).addE(b.of(LABEL, "brother")).from("a").next();
        g.V(b.of(OUT_V, jupiter)).as("a").V(b.of(IN_V, pluto)).addE(b.of(LABEL, "brother")).from("a").next();

        g.V(b.of(OUT_V, neptune)).as("a").V(b.of(IN_V, sea)).addE(b.of(LABEL, "lives"))
                .property(REASON, b.of(REASON, "loves waves")).from("a").next();
        g.V(b.of(OUT_V, neptune)).as("a").V(b.of(IN_V, jupiter)).addE(b.of(LABEL, "brother")).from("a").next();
        g.V(b.of(OUT_V, neptune)).as("a").V(b.of(IN_V, pluto)).addE(b.of(LABEL, "brother")).from("a").next();

        g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, jupiter)).addE(b.of(LABEL, "father")).from("a").next();
        g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, alcmene)).addE(b.of(LABEL, "mother")).from("a").next();

        if (supportsGeoshape) {
            g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, nemean)).addE(b.of(LABEL, "battled"))
                    .property(TIME, b.of(TIME, 1)).property(PLACE, b.of(PLACE, Geoshape.point(38.1f, 23.7f))).from("a")
                    .next();
            g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, hydra)).addE(b.of(LABEL, "battled"))
                    .property(TIME, b.of(TIME, 2)).property(PLACE, b.of(PLACE, Geoshape.point(37.7f, 23.9f))).from("a")
                    .next();
            g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, cerberus)).addE(b.of(LABEL, "battled"))
                    .property(TIME, b.of(TIME, 12)).property(PLACE, b.of(PLACE, Geoshape.point(39f, 22f))).from("a")
                    .next();
        } else {
            g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, nemean)).addE(b.of(LABEL, "battled"))
                    .property(TIME, b.of(TIME, 1)).property(PLACE, b.of(PLACE, getGeoFloatArray(38.1f, 23.7f)))
                    .from("a").next();
            g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, hydra)).addE(b.of(LABEL, "battled"))
                    .property(TIME, b.of(TIME, 2)).property(PLACE, b.of(PLACE, getGeoFloatArray(37.7f, 23.9f)))
                    .from("a").next();
            g.V(b.of(OUT_V, hercules)).as("a").V(b.of(IN_V, cerberus)).addE(b.of(LABEL, "battled"))
                    .property(TIME, b.of(TIME, 12)).property(PLACE, b.of(PLACE, getGeoFloatArray(39f, 22f))).from("a")
                    .next();
        }

        g.V(b.of(OUT_V, pluto)).as("a").V(b.of(IN_V, jupiter)).addE(b.of(LABEL, "brother")).from("a").next();
        g.V(b.of(OUT_V, pluto)).as("a").V(b.of(IN_V, neptune)).addE(b.of(LABEL, "brother")).from("a").next();
        g.V(b.of(OUT_V, pluto)).as("a").V(b.of(IN_V, tartarus)).addE(b.of(LABEL, "lives"))
                .property(REASON, b.of(REASON, "no fear of death")).from("a").next();
        g.V(b.of(OUT_V, pluto)).as("a").V(b.of(IN_V, cerberus)).addE(b.of(LABEL, "pet")).from("a").next();

        g.V(b.of(OUT_V, cerberus)).as("a").V(b.of(IN_V, tartarus)).addE(b.of(LABEL, "lives")).from("a").next();
    }

    private float[] getGeoFloatArray(final float lat, final float lon) {
        final float[] fa = {lat, lon};
        return fa;
    }

    public void createIndex() {
        String commond =
                " JanusGraphManagement mgmt = graph.openManagement();" +
                        "        PropertyKey City = mgmt.getPropertyKey(\"City\");" +
                        "        mgmt.buildIndex(\"cityIndex\", Vertex.class)" +
                        "                .addKey(City)" +
                        "                .buildCompositeIndex();" +
                        "mgmt.commit();";
        final ResultSet resultSet = client.submit(commond);
        Stream<Result> futureList = resultSet.stream();
        futureList.map(result -> result.toString()).forEach(LOGGER::info);
    }

    public void reindexData() {
        String commond =
                " JanusGraphManagement mgmt = graph.openManagement();" +
//                        "mgmt.updateIndex(mgmt.getGraphIndex(\"weatherAndPerson\"), SchemaAction.REINDEX).get();" +
                        "mgmt.updateIndex(mgmt.getGraphIndex(\"dateIndex\"), SchemaAction.REINDEX).get();" +
                        "mgmt.updateIndex(mgmt.getGraphIndex(\"cityIndex\"), SchemaAction.REINDEX).get();" +
                        "mgmt.commit();";
        final ResultSet resultSet = client.submit(commond);
        Stream<Result> futureList = resultSet.stream();
        futureList.map(result -> result.toString()).forEach(LOGGER::info);
    }

    public void readElements() throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (g == null) {
                return;
            }

            LOGGER.info("reading elements");

            // look up vertex by name can use a composite index in JanusGraph
            Optional<Map<String, Object>> v = g.V().has("City", "济南").valueMap(true).tryNext();
//            String person = g.V().has("personalName", "赵平").outE("father").inV().values("name").next().toString();
            List<Edge> allNodes = g.V().has("personalName", eq("赵平")).outE().toList();
//            LOGGER.info("王文淇的父亲是: {}", person);
            LOGGER.info("王文淇的所有相关节点: {}", allNodes);
            if (v.isPresent()) {
                LOGGER.info(v.get().toString());
            } else {
                LOGGER.warn("jupiter not found");
            }
            List<Vertex> count = g.V().has("City", eq("济宁"))
                    .has("qualityGrade", "重度污染")
                    .has("pm25", gt(100))
                    .order().by("pm25", decr).limit(10).toList();

            LOGGER.info(sdf.parse("2016-01-01").toString());

            long a = g.V().has("City", "济南").has("qualityGrade", "良").count().next();

            //long b = g.V().has("date", between(sdf.parse("2016-01-01").getTime(), sdf.parse("2017-01-01").getTime())).count().next();
            LOGGER.info("济南天气良的天数 {}", a);
            LOGGER.info("济宁天气重度污染天数 {}", count);
            //LOGGER.info("2016-01-01 到 2017-01-01的天数 {}", b);
        } finally {
            // the default behavior automatically starts a transaction for
            // any graph interaction, so it is best to finish the transaction
            // even for read-only graph query operations
            if (supportsTransactions) {
                g.tx().rollback();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        List<String> datagram = FileUtils.readLines(new File("/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/pm25新.csv"), "gbk");
        datagram.remove(0);
        final JanusGraphApp app = new JanusGraphApp("/Users/Diablo/Documents/2017workspace/test-janusgraph/src/main/resources/jgex-remote.properties");
        app.openGraph();
        //app.addElementForTest();
        SchemaManager<String, String> sc = new RequestSchemaManager();
        app.createSchema(sc);

        for (final String s : datagram) {
            app.createElements(s);
        }

        //app.reindexData();
//        List<Object> brothers = app.g.V().has("name", "jupiter").both("brother").values("name").dedup().toList();
//        LOGGER.info("jupiter's brothers: " + brothers.toString());
        // long count=app.g.V().count().next();
        // LOGGER.info("graph Vertex count {}",count);
//        app.reindexData();
//        app.readElements();
        app.closeGraph();
        System.exit(0);
    }
}
