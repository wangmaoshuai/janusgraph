package com.janusgraph;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.util.GraphFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Diablo on 2017/10/24.
 * 关系图本地连接获取
 */
public class GraphLocalConnection extends GraphConnection {
    private static final Logger LOGGER = LoggerFactory.getLogger(GraphLocalConnection.class);

    public GraphLocalConnection(final String propFileName) {
        super(propFileName);
    }

    @Override
    public GraphTraversalSource openGraph() throws ConfigurationException {
        LOGGER.info("opening graph");
        Configuration conf = new PropertiesConfiguration(propFileName);
        graph = GraphFactory.open(conf);
        g = graph.traversal();
        return g;
    }

    @Override
    public void closeGraph() throws Exception {
        LOGGER.info("closing graph");
        try {
            if (g != null) {
                g.close();
            }
            if (graph != null) {
                graph.close();
            }
        } finally {
            g = null;
            graph = null;
        }
    }
}
