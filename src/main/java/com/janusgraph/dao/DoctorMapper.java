package com.janusgraph.dao;

import com.janusgraph.dao.imethod.IDoctorMapper;
import com.janusgraph.data.Doctor;
import com.janusgraph.util.OracleUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class DoctorMapper {
    static SqlSessionFactory sqlSessionFactory = null;

    static {
        sqlSessionFactory = OracleUtil.getSqlSessionFactory();
    }
    public List<Doctor> getAllDoctors() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            IDoctorMapper IDischargeMapper = sqlSession.getMapper(IDoctorMapper.class);
            List<Doctor> result = IDischargeMapper.getAllDoctors();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }
}
