package com.janusgraph.dao;

import com.janusgraph.dao.imethod.IAdmissionMapper;
import com.janusgraph.data.AdmissionBean;
import com.janusgraph.util.OracleUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public class AdmissionMapper {
    static SqlSessionFactory sqlSessionFactory = null;

    static {
        sqlSessionFactory = OracleUtil.getSqlSessionFactory();
    }

    public List<AdmissionBean> getAdmissionBean() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            IAdmissionMapper IAdmissionMapper = sqlSession.getMapper(IAdmissionMapper.class);
            List<AdmissionBean> result = IAdmissionMapper.getAllAdmmissions();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }
}
