package com.janusgraph.dao;

import com.janusgraph.dao.imethod.IOutpatientMapper;
import com.janusgraph.data.OutpatientBean;
import com.janusgraph.util.OracleUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public class OutPatientMapper {
    static SqlSessionFactory sqlSessionFactory = null;

    static {
        sqlSessionFactory = OracleUtil.getSqlSessionFactory();
    }

    public List<OutpatientBean> getOutPatientBean() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            IOutpatientMapper IOutpatientMapper = sqlSession.getMapper(IOutpatientMapper.class);
            List<OutpatientBean> result = IOutpatientMapper.getAllOutPatient();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }
}
