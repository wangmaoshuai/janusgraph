package com.janusgraph.dao.imethod;

import com.janusgraph.data.AdmissionBean;
import com.janusgraph.data.OutpatientRecipeBean;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public interface IOutpatientRecipeMapper {
    List<OutpatientRecipeBean> getAllOutpatientRecipe();
}
