package com.janusgraph.dao.imethod;


import com.janusgraph.data.OutpatientBean;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public interface IOutpatientMapper {
    List<OutpatientBean> getAllOutPatient();
}
