package com.janusgraph.dao.imethod;

import com.janusgraph.data.DischargeBean;
import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public interface IDischargeMapper {
    List<DischargeBean> getAllDischarge();
}
