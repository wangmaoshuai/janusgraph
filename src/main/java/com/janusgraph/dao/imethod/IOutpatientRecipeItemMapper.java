package com.janusgraph.dao.imethod;

import com.janusgraph.data.OutpatientRecipeItemBean;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public interface IOutpatientRecipeItemMapper {
    List<OutpatientRecipeItemBean> getAllOutpatientRecipeItem();
}
