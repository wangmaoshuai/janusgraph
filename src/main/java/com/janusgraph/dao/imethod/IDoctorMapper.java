package com.janusgraph.dao.imethod;

import com.janusgraph.data.Doctor;

import java.util.List;

public interface IDoctorMapper {
    List<Doctor> getAllDoctors();
}
