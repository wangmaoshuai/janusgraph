package com.janusgraph.dao;

import com.janusgraph.dao.imethod.IDischargeMapper;
import com.janusgraph.dao.imethod.IIchMapper;
import com.janusgraph.data.AdmissionBean;
import com.janusgraph.data.DischargeBean;
import com.janusgraph.data.IchBean;
import com.janusgraph.util.OracleUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public class IchMapper {
    static SqlSessionFactory sqlSessionFactory = null;

    static {
        sqlSessionFactory = OracleUtil.getSqlSessionFactory();
    }

    public List<IchBean> getIchBean() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            IIchMapper IIchMapper = sqlSession.getMapper(IIchMapper.class);
            List<IchBean> result = IIchMapper.getAllIch();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }
}
