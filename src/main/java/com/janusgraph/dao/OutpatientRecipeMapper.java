package com.janusgraph.dao;

import com.janusgraph.dao.imethod.IOutpatientRecipeMapper;
import com.janusgraph.data.OutpatientRecipeBean;
import com.janusgraph.util.OracleUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public class OutpatientRecipeMapper {
    static SqlSessionFactory sqlSessionFactory = null;

    static {
        sqlSessionFactory = OracleUtil.getSqlSessionFactory();
    }

    public List<OutpatientRecipeBean> getOutpatienRecipeBean() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            IOutpatientRecipeMapper IOutpatientRecipeMapper = sqlSession.getMapper(IOutpatientRecipeMapper.class);
            List<OutpatientRecipeBean> result = IOutpatientRecipeMapper.getAllOutpatientRecipe();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }
}
