package com.janusgraph.dao;

import com.janusgraph.dao.imethod.IDischargeMapper;
import com.janusgraph.data.DischargeBean;
import com.janusgraph.util.OracleUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

/**
 * Created by Diablo on 2017/10/26.
 */
public class DischargeMapper {
    static SqlSessionFactory sqlSessionFactory = null;

    static {
        sqlSessionFactory = OracleUtil.getSqlSessionFactory();
    }

    public List<DischargeBean> getDischargeBean() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            IDischargeMapper IDischargeMapper = sqlSession.getMapper(IDischargeMapper.class);
            List<DischargeBean> result = IDischargeMapper.getAllDischarge();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return null;
    }
}
