package com.janusgraph.local;

import com.janusgraph.GraphConnection;
import com.janusgraph.GraphLocalConnection;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Edge;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.RelationType;
import org.janusgraph.core.attribute.Geoshape;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.janusgraph.core.schema.Mapping;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Diablo on 2017/9/28.
 * 使用本地链接janusGraph
 */
public class LocalApp {
    private static final Logger LOGGER = Logger.getLogger(LocalApp.class);
    private GraphTraversalSource g;
    private boolean supportsTransactions;
    private boolean supportsGeoshape;
    private boolean useMixedIndex;
    private static final String mixedIndexConfigName = "search";
    private final GraphConnection graphLocalConnection;

    public LocalApp(String propFileName) {
        this.supportsGeoshape = true;
        this.supportsTransactions = true;
        this.useMixedIndex = true;
        graphLocalConnection = new GraphLocalConnection(propFileName);
    }

    public GraphTraversalSource openGraph() throws ConfigurationException {
        LOGGER.info("opening graph");
        g = graphLocalConnection.openGraph();
        return g;
    }

    public void closeGraph() throws Exception {
        LOGGER.info("closing graph");
        graphLocalConnection.closeGraph();
    }

    //删除graph并没有实现
    public void dropGraph() throws Exception {
        graphLocalConnection.dropGraph();
    }

    //创建schema
    public void createSchema() {
        final JanusGraphManagement mgmt = graphLocalConnection.getGraph().get().openManagement();
        try {
//            if (mgmt.getRelationTypes(RelationType.class).iterator().hasNext()) {
//                mgmt.rollback();
//                return;
//            }
            LOGGER.info("creating schema");
            createProperties(mgmt);
            createVertexLabels(mgmt);
            createEdgeLabels(mgmt);
            createCompositeIndexes(mgmt);
            createMixedIndexes(mgmt);
            mgmt.commit();

        } catch (Exception e) {
            e.printStackTrace();
            mgmt.rollback();
        }
    }

    public void createProperties(final JanusGraphManagement mgmt) {
        mgmt.makePropertyKey("name").dataType(String.class).make();
        mgmt.makePropertyKey("age").dataType(Integer.class).make();
        mgmt.makePropertyKey("time").dataType(Integer.class).make();
        mgmt.makePropertyKey("reason").dataType(String.class).make();
        mgmt.makePropertyKey("place").dataType(Geoshape.class).make();
    }

    public void createEdgeLabels(final JanusGraphManagement mgmt) {
        mgmt.makeEdgeLabel("father").multiplicity(Multiplicity.MANY2ONE).make();
        mgmt.makeEdgeLabel("mother").multiplicity(Multiplicity.MANY2ONE).make();
        mgmt.makeEdgeLabel("lives").signature(mgmt.getPropertyKey("reason")).make();
        mgmt.makeEdgeLabel("pet").make();
        mgmt.makeEdgeLabel("brother").make();
        mgmt.makeEdgeLabel("battled").make();
    }

    public void createVertexLabels(final JanusGraphManagement mgmt) {
        mgmt.makeVertexLabel("titan").make();
        mgmt.makeVertexLabel("location").make();
        mgmt.makeVertexLabel("god").make();
        mgmt.makeVertexLabel("demigod").make();
        mgmt.makeVertexLabel("human").make();
        mgmt.makeVertexLabel("monster").make();
    }

    public void createMixedIndexes(final JanusGraphManagement mgmt) {
        if (useMixedIndex) {
            mgmt.buildIndex("vAge", Vertex.class).addKey(mgmt.getPropertyKey("age"))
                    .buildMixedIndex(mixedIndexConfigName);
            mgmt.buildIndex("eReasonPlace", Edge.class).addKey(mgmt.getPropertyKey("reason"))
                    .addKey(mgmt.getPropertyKey("place")).buildMixedIndex(mixedIndexConfigName);
        }
    }

    public void createCompositeIndexes(final JanusGraphManagement mgmt) {
        mgmt.buildIndex("nameIndex", Vertex.class).addKey(mgmt.getPropertyKey("name")).buildCompositeIndex();
    }

    public void deleteElements() {
        try {
            if (g == null) {
                return;
            }
            LOGGER.info("deleting elements");
            // note that this will succeed whether or not pluto exists
            g.V().has("name", "pluto").drop().iterate();
            if (supportsTransactions) {
                g.tx().commit();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            if (supportsTransactions) {
                g.tx().rollback();
            }
        }
    }

    public void createElements() {
        try {
            // naive check if the graph was previously created
            if (g.V().has("name", "saturn").hasNext()) {
                if (supportsTransactions) {
                    g.tx().rollback();
                }
                return;
            }
            LOGGER.info("creating elements");

            // see GraphOfTheGodsFactory.java

            final Vertex saturn = g.addV("titan").property("name", "saturn").property("age", 10000).next();
            final Vertex sky = g.addV("location").property("name", "sky").next();
            final Vertex sea = g.addV("location").property("name", "sea").next();
            final Vertex jupiter = g.addV("god").property("name", "jupiter").property("age", 5000).next();
            final Vertex neptune = g.addV("god").property("name", "neptune").property("age", 4500).next();
            final Vertex hercules = g.addV("demigod").property("name", "hercules").property("age", 30).next();
            final Vertex alcmene = g.addV("human").property("name", "alcmene").property("age", 45).next();
            final Vertex pluto = g.addV("god").property("name", "pluto").property("age", 4000).next();
            final Vertex nemean = g.addV("monster").property("name", "nemean").next();
            final Vertex hydra = g.addV("monster").property("name", "hydra").next();
            final Vertex cerberus = g.addV("monster").property("name", "cerberus").next();
            final Vertex tartarus = g.addV("location").property("name", "tartarus").next();

            g.V(jupiter).as("a").V(saturn).addE("father").from("a").next();
            g.V(jupiter).as("a").V(sky).addE("lives").property("reason", "loves fresh breezes").from("a").next();
            g.V(jupiter).as("a").V(neptune).addE("brother").from("a").next();
            g.V(jupiter).as("a").V(pluto).addE("brother").from("a").next();

            g.V(neptune).as("a").V(sea).addE("lives").property("reason", "loves waves").from("a").next();
            g.V(neptune).as("a").V(jupiter).addE("brother").from("a").next();
            g.V(neptune).as("a").V(pluto).addE("brother").from("a").next();

            g.V(hercules).as("a").V(jupiter).addE("father").from("a").next();
            g.V(hercules).as("a").V(alcmene).addE("mother").from("a").next();

            if (supportsGeoshape) {
                g.V(hercules).as("a").V(nemean).addE("battled").property("time", 1)
                        .property("place", Geoshape.point(38.1f, 23.7f)).from("a").next();
                g.V(hercules).as("a").V(hydra).addE("battled").property("time", 2)
                        .property("place", Geoshape.point(37.7f, 23.9f)).from("a").next();
                g.V(hercules).as("a").V(cerberus).addE("battled").property("time", 12)
                        .property("place", Geoshape.point(39f, 22f)).from("a").next();
            } else {
                g.V(hercules).as("a").V(nemean).addE("battled").property("time", 1)
                        .property("place", getGeoFloatArray(38.1f, 23.7f)).from("a").next();
                g.V(hercules).as("a").V(hydra).addE("battled").property("time", 2)
                        .property("place", getGeoFloatArray(37.7f, 23.9f)).from("a").next();
                g.V(hercules).as("a").V(cerberus).addE("battled").property("time", 12)
                        .property("place", getGeoFloatArray(39f, 22f)).from("a").next();
            }

            g.V(pluto).as("a").V(jupiter).addE("brother").from("a").next();
            g.V(pluto).as("a").V(neptune).addE("brother").from("a").next();
            g.V(pluto).as("a").V(tartarus).addE("lives").property("reason", "no fear of death").from("a").next();
            g.V(pluto).as("a").V(cerberus).addE("pet").from("a").next();

            g.V(cerberus).as("a").V(tartarus).addE("lives").from("a").next();

            if (supportsTransactions) {
                g.tx().commit();
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            if (supportsTransactions) {
                g.tx().rollback();
            }
        }
    }

    protected float[] getGeoFloatArray(final float lat, final float lon) {
        final float[] fa = {lat, lon};
        return fa;
    }

    public void readElements() {
        try {
            if (g == null) {
                return;
            }

            LOGGER.info("reading elements");

            // look up vertex by name can use a composite index in JanusGraph
            final Optional<Map<String, Object>> v = g.V().has("name", "jupiter").valueMap(true).tryNext();
            if (v.isPresent()) {
                LOGGER.info(v.get().toString());
            } else {
                LOGGER.warn("jupiter not found");
            }

            // look up an incident edge
            final Optional<Map<String, Object>> edge = g.V().has("name", "hercules").outE("battled").as("e").inV()
                    .has("name", "hydra").select("e").valueMap(true).tryNext();
            if (edge.isPresent()) {
                LOGGER.info(edge.get().toString());
            } else {
                LOGGER.warn("hercules battled hydra not found");
            }

            // numerical range query can use a mixed index in JanusGraph
            final List<Object> list = g.V().has("age", P.gte(5000)).values("age").toList();
            LOGGER.info(list.toString());

            // pluto might be deleted
            final boolean plutoExists = g.V().has("name", "pluto").hasNext();
            if (plutoExists) {
                LOGGER.info("pluto exists");
            } else {
                LOGGER.warn("pluto not found");
            }

            // look up jupiter's brothers
            final List<Object> brothers = g.V().has("name", "jupiter").both("brother").values("name").dedup().toList();
            LOGGER.info("jupiter's brothers: " + brothers.toString());

        } finally {
            // the default behavior automatically starts a transaction for
            // any graph interaction, so it is best to finish the transaction
            // even for read-only graph query operations
            if (supportsTransactions) {
                g.tx().rollback();
            }
        }
    }
}
